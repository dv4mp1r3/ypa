import os
from abc import ABC, abstractmethod
from aiohttp import web
import subprocess


class AbstractWrapper(ABC):
    @property
    def use_cli(self) -> bool:
        return

    def __init__(self, use_cli: bool):
        self.__class__.use_cli = use_cli

    @classmethod
    @abstractmethod
    def get_command(cls) -> str:
        pass

    @classmethod
    @abstractmethod
    def validate_input(cls, request):
        pass

    @abstractmethod
    def get_route(self) -> str:
        pass

    @classmethod
    async def handle_request(cls, request) -> web.Response:
        cls.validate_input(request)
        result, is_error = cls.execute(cls.get_command())
        if is_error:
            return web.Response(text=result.decode("utf-8"), status=500)
        return web.Response(text=result.decode("utf-8"))

    @classmethod
    def execute(cls, command: str) -> str:
        process = subprocess.Popen(command,
                                   shell=True,
                                   executable='/bin/bash',
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        if len(stderr) > 0:
            return stderr+stdout, True
        return stdout, False


class WpscanWrapper(AbstractWrapper):

    @classmethod
    def validate_input(cls, request):
        cls.domain = request.match_info.get('domain', "Anonymous")

    def get_route(self) -> str:
        return '/wpscan/{domain}'

    @property
    def domain(self) -> str:
        return

    @classmethod
    def get_command(cls) -> str:
        args = f'--url https://{cls.domain}/ --enumerate u'
        if cls.use_cli:
            return f'wpscan {args}'
        return f'docker run -i --rm wpscanteam/wpscan {args}'


class NmapWrapper(AbstractWrapper):
    @property
    def host(self) -> str:
        return

    @classmethod
    def get_command(cls) -> str:
        if cls.use_cli:
            return f'nmap {cls.host}'
        return f'docker run --rm -i instrumentisto/nmap {cls.host}'

    @classmethod
    def validate_input(cls, request):
        cls.host = request.match_info.get('host', "Anonymous")

    def get_route(self) -> str:
        return '/nmap/{host}'


class SubfinderWrapper(AbstractWrapper):
    @property
    def domain(self) -> str:
        return

    @classmethod
    def execute(cls, command: str) -> str:
        process = subprocess.Popen(command,
                                   shell=True,
                                   executable='/bin/bash',
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        if len(stderr) > 0:
            str_stderr = stderr.decode("utf-8")
            if str_stderr.endswith(f'Enumerating subdomains for {cls.domain}\n'):
                return stderr + stdout, False
            return stderr + stdout, True
        return stdout, False

    @classmethod
    def get_command(cls) -> str:
        if cls.use_cli:
            return f'subfinder -d {cls.domain}'
        return f'docker run --rm -i ice3man/subfinder:v2.0 -d {cls.domain}'

    @classmethod
    def validate_input(cls, request):
        cls.domain = request.match_info.get('domain', "Anonymous")

    def get_route(self) -> str:
        return '/subfinder/{domain}'


class VulnersWrapper(NmapWrapper):

    def get_route(self) -> str:
        return '/vulners/{host}'

    @classmethod
    def get_command(cls) -> str:
        if cls.use_cli:
            return f'nmap -sV --script vulners {cls.host}'
        # todo: implement
        return f''


class HeartbleederWrapper(NmapWrapper):
    @property
    def port(self) -> str:
        return

    @property
    def host(self) -> str:
        return

    @classmethod
    def get_command(cls) -> str:
        if cls.use_cli:
            return f'nmap --script=ssl-heartbleed -p {cls.port} {cls.host}'
        # todo: implement
        return f''

    @classmethod
    def validate_input(cls, request):
        cls.port = request.match_info.get('port', "Anonymous")
        cls.host = request.match_info.get('host', "Anonymous")
        super().validate_input(cls, request)

    def get_route(self) -> str:
        return '/heartbleeder/{host}/{port}'


class PingWrapper(AbstractWrapper):
    @property
    def host(self) -> str:
        return

    @classmethod
    def get_command(cls) -> str:
        if cls.use_cli:
            return f'ping {cls.host} -c 1'
        return f'docker run --rm -i willfarrell/ping {cls.host} -c 1'

    @classmethod
    def validate_input(cls, request):
        cls.host = request.match_info.get('host', "Anonymous")

    def get_route(self) -> str:
        return '/ping/{host}'


class HydraWrapper(AbstractWrapper):
    @property
    def host(self) -> str:
        return

    @property
    def user_list(self) -> str:
        return

    @property
    def pwd_list(self) -> str:
        return

    @classmethod
    def get_command(cls) -> str:
        if cls.use_cli:
            return f'hydra -L {cls.user_list} -P {cls.pwd_list} {cls.host} -t 4'
        return f'docker run --rm -i oryd/hydra:v1.10.7-alpine {cls.host}'

    @classmethod
    def validate_input(cls, request):
        cls.host = request.match_info.get('host', "Anonymous")
        cls.user_list = request.match_info.get('user_list', "Anonymous")
        cls.pwd_list = request.match_info.get('pwd_list', "Anonymous")

    def get_route(self) -> str:
        return '/hydra/{host}/{user_list}/{pwd_list}'


class HostWrapper(AbstractWrapper):
    @property
    def host(self) -> str:
        return

    @classmethod
    def get_command(cls) -> str:
        if cls.use_cli:
            return f'host {cls.host}'
        # todo: implement
        return f''

    @classmethod
    def validate_input(cls, request):
        cls.host = request.match_info.get('host', "Anonymous")

    def get_route(self) -> str:
        return '/host/{host}'


class DirbWrapper(AbstractWrapper):
    @property
    def host(self) -> str:
        return

    @property
    def protocol(self) -> str:
        return

    @property
    def dict_name(self) -> str:
        return

    @classmethod
    def get_command(cls) -> str:
        if cls.use_cli:
            return f'dirb {cls.protocol}://{cls.host} /usr/share/wordlists/dirb/{cls.dict_name}'
        # todo: implement
        return f''

    @classmethod
    def validate_input(cls, request):
        cls.host = request.match_info.get('host', "Anonymous")
        cls.protocol = request.match_info.get('protocol', "Anonymous")
        cls.dict_name = request.match_info.get('dict_name', "Anonymous")

    def get_route(self) -> str:
        return '/dirb/{protocol}/{host}/{dict_name}'


class DnsReconWrapper(AbstractWrapper):
    @property
    def domain(self) -> str:
        return

    @property
    def out(self) -> str:
        return

    @classmethod
    def get_command(cls) -> str:
        cls.out = f'/tmp/{cls.domain}.json'
        if cls.use_cli:
            return f'dnsrecon -d {cls.domain} -j {cls.out}'
        return f'docker run --rm -i dnsrecon:test -d {cls.domain}'

    @classmethod
    def validate_input(cls, request):
        cls.domain = request.match_info.get('domain', "Anonymous")

    def get_route(self) -> str:
        return '/dnsrecon/{domain}'

    @classmethod
    def execute(cls, command: str) -> bytes:
        output, result = super().execute(command)
        if os.path.isfile(cls.out):
            with open(cls.out, 'r') as f:
                json_out = f.read()
            os.remove(cls.out)
        if len(json_out) > 0:
            return str.encode(json_out), result
        return output, result


class PmaWrapper(AbstractWrapper):
    @property
    def domain(self) -> str:
        return

    @property
    def protocol(self) -> str:
        return

    @classmethod
    def get_command(cls) -> str:
        if cls.use_cli:
            return f'websploit web/pma {cls.protocol}://{cls.domain}'
        # todo: implement
        return f''

    @classmethod
    def validate_input(cls, request):
        cls.domain = request.match_info.get('domain', "Anonymous")
        cls.protocol = request.match_info.get('protocol', "Anonymous")

    def get_route(self) -> str:
        return '/pma/{protocol}/{domain}'
