FROM kalilinux/kali:amd64 AS kali-base

WORKDIR /var/python

COPY ./python/requirements.txt .

RUN echo 'Acquire::https::mirror-1.truenetwork.ru::Verify-Peer "false";' > /etc/apt/apt.conf.d/99kalizalupa \
    && apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y python3-pip \
    && pip3 install -r requirements.txt

FROM kali-base AS kali-complex

RUN apt update \
    && apt install -y nmap wpscan subfinder whois git inetutils-ping hydra host dirb wordlists websploit \
    && git clone https://github.com/vulnersCom/nmap-vulners.git \
    && git clone https://github.com/sensepost/heartbleed-poc.git \
    && mkdir -p /root/.nmap/scripts \
    && cp nmap-vulners/*.* /root/.nmap/scripts/ \
    && cp heartbleed-poc/*.* /root/.nmap/scripts/ \
    && apt-get autoclean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/{apt,dpkg,cache,log}/ \
    && rm -rf /tmp/* /var/tmp/*

CMD ["python3", "main.py"]
