from aiohttp import web
import os
import argparse
from importlib import import_module
import pyclbr


def start_commands_in_cli(use_cli: bool) -> bool:
    env_var = os.environ.get('USE_CLI')
    return use_cli is True or env_var is not None and int(env_var) == 1


if __name__ == '__main__':
    parser = argparse.ArgumentParser("")
    parser.add_argument('-u', '--use_cli', help="", type=bool, required=False)
    args = parser.parse_args()
    use_cli = start_commands_in_cli(args.use_cli)
    app = web.Application()
    module_name = 'container_wrappers.wrappers'
    module_info = pyclbr.readmodule(module_name)
    module = import_module(module_name)
    for class_name in module_info:
        if class_name == 'AbstractWrapper':
            continue
        class_ = getattr(module, class_name)
        instance = class_(use_cli)
        app.add_routes([web.get(instance.get_route(), instance.handle_request)])
    web.run_app(app, port=5555)
