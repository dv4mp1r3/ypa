all: run1 run2

run1: ## Create rabbit_definitions & cookie_validation
	cd configs && python3 rabbit_definitions_gen.py && python3 cookie_validation_key_gen.py

run2: ## Running docker-compose:
	docker-compose -f deploy-docker-compose.yaml up
