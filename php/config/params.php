<?php

return [
    'adminEmail' => 'admin@example.com',
    'rabbit' => require_once 'rabbit.php',
    'bsDependencyEnabled' => false,
    'isDemo' => intval(getenv('IS_DEMO')),
];
