<?php

return [
    'host' => 'rabbit',
    'port' => 5672,
    'vhost' => getenv('RABBITMQ_DEFAULT_VHOST'),
    'user' => getenv('RABBITMQ_DEFAULT_USER'),
    'password' => getenv('RABBITMQ_DEFAULT_PASS'),
    'queue' => 'execute',
];