<?php

declare(strict_types=1);

namespace app\assets;

use yii\web\AssetBundle;

class SiteAboutAsset extends AssetBundle
{
    public $js = [
        'js/site-about.js',
    ];
    public $depends = [
        BpmnAsset::class
    ];
}