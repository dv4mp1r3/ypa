<?php

namespace app\assets;

class BootstrapAssetBundle extends \yii\web\AssetBundle
{
    public $sourcePath = '@bower/bootstrap';
    public $js = [
        'dist/js/bootstrap.min.js',
    ];
}