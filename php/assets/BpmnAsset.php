<?php

declare(strict_types=1);

namespace app\assets;

use yii\web\AssetBundle;

class BpmnAsset extends AssetBundle
{
    public $js = [
        'js/bpmn-viewer.js',
    ];
    public $depends = [
        SbAppAsset::class
    ];

}