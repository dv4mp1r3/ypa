<?php

declare(strict_types=1);

namespace app\commands;

use app\models\Task;
use app\models\Notification;
use app\models\auth\User;
use yii\console\Controller;
use Faker\Factory;
use Faker\Generator;

class TestDataController extends Controller
{
    private array $userIds = [];

    private array $taskIds = [];

    private array $notificationIds = [];

    private Generator $faker;

    public function __construct($id, $module, $config = [])
    {
        $this->faker = Factory::create();
        parent::__construct($id, $module, $config);
    }

    /**
     * Добавление нового пользователя, задач и уведомлений к задачам
     * @throws \Exception
     */
    public function actionAddAll(): void
    {
        $this->actionAddUsers();
        $this->actionAddTasks();
        $this->actionAddNotifications();
    }

    /**
     * Добавление новых пользователей
     * @param int $count количество
     * @return array
     */
    public function actionAddUsers(int $count = 1): array
    {
        for($i = 0; $i < $count; $i++) {
            $user = new User();
            $user->is_admin = false;
            $user->name = strtolower($this->faker->firstName);
            $password = $this->faker->password(6, 8);
            $user->password = $password;
            if (!$user->save()) {
                echo "Errors on creating new username {$user->name}:".PHP_EOL;
                echo var_export($user->errors, true);
                echo PHP_EOL;
            }
            else {
                echo "Added new user {$user->name} with password {$password}".PHP_EOL;
                $this->userIds[] = $user->id;
            }
        }
        return $this->userIds;
    }

    /**
     * Добавление новых задач
     * @param int $count количество
     * @return array
     * @throws \Exception
     */
    public function actionAddTasks(int $count = 10): array
    {
        if (count($this->userIds) === 0) {
            $this->actionAddUsers();
        }
        for($i = 0; $i < $count; $i++) {
            $task = new Task();
            $userId = $this->userIds[array_rand($this->userIds)];
            $task->name = str_replace(' ', '', $this->faker->name);
            $task->filename = '/var/www/ypa.local/uploads/'.md5(time().rand(0, 1000));
            //todo: fix
            //$this->faker->file('/tmp', '/var/www/ypa.local/uploads');
            if (!$task->save()) {
                echo "Errors on creating new task {$task->name}:".PHP_EOL;
                echo var_export($task->errors, true);
                echo PHP_EOL;
            } else {
                Task::updateAll(['user_id' => $userId], ['id' => $task->id]);
                echo "Added new task {$task->name} with id $task->id".PHP_EOL;
                $this->taskIds[] = $task->id;
            }
        }
        return $this->taskIds;
    }

    /**
     * Добавление новых уведомлений
     * @param int $count количество
     * @return array
     */
    public function actionAddNotifications(int $count = 100): array
    {
        if (count($this->taskIds) === 0) {
            $this->actionAddTasks();
        }
        for($i = 0; $i < $count; $i++) {
            $n = new Notification();
            $n->task_id = $this->taskIds[array_rand($this->taskIds)];
            $n->text = $this->faker->realText(100);
            $n->extra = $this->faker->realText(100);
            $n->short_info = $this->faker->text(50);
            $n->type = rand(Notification::TYPE_DOS, Notification::TYPE_FILE_INCLUSION);
            $n->level = rand(Notification::LEVEL_LOW, Notification::LEVEL_CRITICAL);
            $n->command = '';
            if (!$n->save()) {
                echo "Errors on creating new notification {$n->id} for task with id {$n->task_id}:".PHP_EOL;
                echo var_export($n->errors, true);
                echo PHP_EOL;
            } else {
                echo "Added new notification {$n->id} for task with id {$n->task_id}".PHP_EOL;
                $this->notificationIds[] = $n->id;
            }
        }
        return $this->notificationIds;
    }

    /**
     * Очистка данных (уведомления, таски, все пользователи, кроме админа)
     * @throws \yii\db\Exception
     */
    public function actionClearData(): void
    {
        $db = Task::getDb();
        $queryBuilder = $db->getQueryBuilder();
        $db->createCommand($queryBuilder->truncateTable(Notification::tableName()))->execute();
        Task::deleteAll();
        User::deleteAll(['!=', 'id', User::ADMIN_USER_ID]);
    }
}