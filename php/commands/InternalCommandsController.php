<?php

declare(strict_types=1);

namespace app\commands;

use app\models\PageSource;
use Yii;
use yii\console\Controller;
use DetectCMS\DetectCMS;


class InternalCommandsController extends Controller
{
    /**
     * Поиск CMS для домена
     * @param string $domain
     *
     * Название CMS в случае если найдена, пустой вывод в противном случае
     */
    public function actionCmsdetect(string $url)
    {
        $cms = new DetectCMS($url);
        if ($cms->check())
        {
            $result = $cms->getResult();
            echo $result.PHP_EOL;
        }
    }

    public function actionLoadFromUrl(string $url)
    {
        $pageSource = new PageSource($url);
        $pageSource->load();
        echo $pageSource->getContent();
        echo PHP_EOL;
    }
}