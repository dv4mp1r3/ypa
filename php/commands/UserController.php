<?php

declare(strict_types=1);

namespace app\commands;

use app\models\auth\User;
use Yii;
use yii\console\Controller;

class UserController extends Controller
{
    /**
     * @param string $name
     * @param string $password
     * @param bool $isAdmin
     */
    public function actionAdd(string $name, string $password, bool $isAdmin)
    {
        $user = new \app\models\auth\User();
        $user->name = $name;
        $user->password = $password;
        $user->disabled = false;
        $user->is_admin = $isAdmin;
        if (!$user->save())
        {
            echo "errors on ".__FUNCTION__."\n";
            $errors = $user->getErrors();
            foreach ($errors as $error)
            {
                echo var_export($errors);
                echo "\n";
            }
        }
        else
        {
            echo "Added user $name (id = $user->id)\n";
        }
    }

    public function actionCheckDemoAccount(): void {
        if (!$this->validateDemoAccountData()) {
            return;
        }

        $username = getenv('YPA_DEMO_USERNAME');
        $user = User::findByUsername($username);
        if (!($user instanceof User)) {
            echo "User '$username' does not exist in database. Creating a new user...".PHP_EOL;
            $user = new User();
            $user->name = $username;
            $user->password = getenv('YPA_DEMO_PASSWORD');
            $user->is_admin = false;
            $user->disabled = false;
            if (!$user->save()) {
                echo "Error on create new user '$username':".PHP_EOL;
                echo print_r($user->getErrors(), true);
                return;
            }
            echo "User '$username' created successfully, id is {$user->id}".PHP_EOL;
            return;
        }
    }

    public function actionDisableNonDemoAccounts() : void {
        if (!$this->validateDemoAccountData()) {
            return;
        }
        $count = User::updateAll(['disabled' => 1], ['!=', 'name', getenv('YPA_DEMO_USERNAME')]);
        echo "Disabled $count accounts".PHP_EOL;
    }

    public function actionEnableNonDemoAccounts() : void {
        if (!$this->validateDemoAccountData()) {
            return;
        }
        $count = User::updateAll(['disabled' => 0], ['!=', 'name', getenv('YPA_DEMO_USERNAME')]);
        echo "Enabled $count accounts".PHP_EOL;
    }

    private function validateDemoAccountData() : bool {
        if (Yii::$app->params['isDemo'] === 0) {
            echo "IS_DEMO env value is not equal to 1".PHP_EOL;
            return false;
        }
        $demoAccountCredentialsEnvs = ['YPA_DEMO_USERNAME', 'YPA_DEMO_PASSWORD'];
        foreach ($demoAccountCredentialsEnvs as $envName) {
            if ($this->checkDemoEnv($envName)) {
                echo "$envName is empty, action will be stop".PHP_EOL;
                return false;
            }
        }
        return true;
    }

    private function checkDemoEnv(string $envName) : bool {
        return empty(getenv($envName));
    }
}