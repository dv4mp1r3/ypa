<?php

declare(strict_types=1);

namespace app\commands;
use app\models\Task;
use Yii;
use yii\base\Module;
use yii\console\Controller;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class TaskController extends Controller
{

    /**
     * Добавление новой задачи с одним доменом
     * @param string $domain
     * @param bool $isApproved если true, задача сразу помещается в очередь
     * @throws \Exception
     */
    public function actionAdd(string $domain, bool $isApproved = false): void
    {
        $model = new Task();
        if ($isApproved) {
            $model->status = Task::STATUS_APPROVED;
        }
        $model->name = $domain.' '.time();
        $model->domains = $domain;
        $filePath = $model->generateTaskFilePath('from-cli', '@app');
        file_put_contents($filePath, $domain);
        $model->filename = $filePath;
        $model->save();
        if ($isApproved) {
            $model->pushToQueue('task');
        }
    }
}