<?php


namespace app\commands;
use app\models\server\LoadRecord;
use app\models\server\Load;
use yii\console\Controller;


class ServerLoadController extends Controller
{
    /**
     * Сборка данных о нагрузке и сохранение в БД
     * @param bool $printAsString вывод информации после сохранения
     */
    public function actionStore(bool $printAsString = false, bool $infinity = false, int $sleepInterval = 1)
    {
        $l = new Load();
        do{
            $object = new LoadRecord();
            $object->setData($l);
            if (!$object->save())
            {
                if ($printAsString)
                {
                    echo "Errors on save:";
                    var_export($object->getErrors());
                }
                $errors = implode("\n", $object->getErrorSummary(true));
                \Yii::error(__METHOD__.": не удалось сохранить модель ".get_class($object).". Ошибки:\n$errors");              
                return;
            }

            $object->refresh();
            if ($printAsString)
            {
                echo $object->toString();
            }
            sleep($sleepInterval);
        }while($infinity);       
    }

    public function actionCleanOld(string $since = 'today')
    {
        if ($since === 'today')
        {
            $since = date_create('now', new \DateTimeZone(\Yii::$app->timeZone))->format('Y-m-d H:i:s');
        }
        $i = LoadRecord::deleteAll(['<', 'creation_date', $since]);
        echo "Removed $i records\n";
    }
    
    /**
     * 
     * @param string $type
     * @param int $limit
     */
    public function actionGetLast(string $type = LoadRecord::TYPE_CPU, int $limit = 10)
    {
        var_export(LoadRecord::findByDateAndType(
                date_create(
                        'now', 
                        new \DateTimeZone(\Yii::$app->timeZone)
                )->format('Y-m-d H:i:s'), 
                $type, 
                $limit));
        echo PHP_EOL;
    }
}