<?php

declare(strict_types=1);

namespace app\commands;

use app\models\Notification;
use app\models\Task;
use Yii;
use yii\console\Controller;

class NotificationController extends Controller
{

    public function actionCreate(
        string $taskIdentity,
        string $shortInfo = null,
        string $text = null,
        string $extra = null,
        string $command = null,
        int $type = 0,
        int $level = 0)
    {

        $task = $this->findTask($taskIdentity);
        if (!($task instanceof Task)) {
            echo "Can't find task with identity {$taskIdentity}".PHP_EOL;
            return;
        }
        $n = new Notification();
        $n->task_id = $task->id;
        $n->text = $text;
        $n->command = $command;
        $n->short_info = $shortInfo;
        $n->extra = $extra;
        $n->level = $level;
        $n->type = $type;
        if (!$n->save()) {
            echo "Errors on create new notification:".PHP_EOL;
            echo print_r($n->errors(), true);
            return;
        }
        echo "Notification created successfully!".PHP_EOL;
    }

    private function findTask(string $taskIdentity) {
        $id = intval($taskIdentity);
        if ($id > 0) {
           return Task::findOne($id);
        }
        return Task::findOne(['name' => $taskIdentity]);
    }

}