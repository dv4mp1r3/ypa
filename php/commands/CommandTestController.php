<?php

declare(strict_types=1);

namespace app\commands;

use app\models\commands\CommandException;
use app\models\runners\CliRunnerSettings;
use app\models\runners\RunnerSettings;
use app\models\runners\WebRunnerSettings;
use PhpAmqpLib\Message\AMQPMessage;
use Yii;
use yii\console\Controller;
use app\models\payloads\TaskProcessor;


class CommandTestController extends Controller
{
    const COMMAND_CONTEXT_WEB = 'web';
    const COMMAND_CONTEXT_CLI = 'cli';

    /**
     * @param string $messageBodyFile имя json файла в каталоге tests/data/
     * @param string $commandContext
     * @throws \Exception
     */
    public function actionRun(string $messageBodyFile = '', string $commandContext = '')
    {
        try {
            if (empty($messageBodyFile)) {
                $messageBodyFile = Yii::getAlias('@app') . '/tests/data/testMessageBody.json';
            } else {
                $messageBodyFile = Yii::getAlias('@app') . "/tests/data/$messageBodyFile.json";
            }
            $object = new AMQPMessage(file_get_contents($messageBodyFile));
            $taskProcessor = new TaskProcessor('');
            $taskProcessor->printResult(true);
            $taskProcessor->setRunnerSettings($this->getRunnerSettings($commandContext));
            $taskProcessor->execute($object);
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    private function getRunnerSettings($cliParam) : ?RunnerSettings {
        switch ($cliParam) {
            case self::COMMAND_CONTEXT_WEB:
                return new WebRunnerSettings(
                    getenv('SERVICE_NAME'),
                    intval(getenv('SERVICE_PORT'))
                );
            case self::COMMAND_CONTEXT_CLI:
                return new CliRunnerSettings();
        }
        throw new CommandException('Unknown runner settings type '.$cliParam);
    }
}