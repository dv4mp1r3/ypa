<?php

declare(strict_types=1);

namespace app\commands;
use app\models\payloads\AbstractPayload;
use app\models\runners\CliRunnerSettings;
use app\models\runners\RunnerSettings;
use app\models\runners\WebRunnerSettings;
use Yii;
use app\models\AMQPConnectionBuilder;
use app\models\AMQPPublisher;
use yii\base\Module;
use yii\console\Controller;
use app\models\AMQPWorker;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use app\models\payloads\TaskProcessor;
use app\models\payloads\NotificationProcessor;
use app\models\commands\AbstractCommand;

class RabbitController extends Controller
{
    const COMMAND_CONTEXT_WEB = 'web';
    const COMMAND_CONTEXT_CLI = 'cli';

    /**
     * @var AMQPWorker
     */
    protected $w;
    
    public function __construct(string $id, Module $module, array $config = array())
    {
        error_reporting(E_ALL & ~E_NOTICE);
        ini_set('display_errors', "1");
        ini_set("memory_limit", "64M");
        $this->w = null;
        parent::__construct($id, $module, $config);
    }
    
    public function actionNotification()
    {
        $name = $this->getQueueName(__FUNCTION__);
        $this->loop($name);
    }

    /**
     * Выполение задачи из очереди
     * @param string|null $queue
     * @throws \Exception
     */
    public function actionTask(string $queue = null)
    {
        $name = $this->getQueueName(__FUNCTION__);
        $this->loop($name, $queue);
    }
    
    /**
     * Получение имени очереди для выполняемой задачи
     * @param string $functionName
     * @return string
     */
    protected function getQueueName(string $functionName): string
    {
        return str_replace('action', '', $functionName);        
    }

    /**
     * Цикл обработки сообщений воркером
     * @param string $payloadName Название класса из неймспейса app\models\payloads
     * инстанс класса AbstractPayload
     * @param string|null $queue название очереди (если не используется, то берется из конфига)
     * @throws \Exception
     * @see app\models\payloads\AbstractPayload
     */
    protected function loop(string $payloadName, string $queue = null)
    {
        $classname = "app\\models\\payloads\\{$payloadName}Processor";

        $connection = $this->getConnection();

        /**
         * @var AbstractPayload $payload
         */
        $payload = new $classname(lcfirst($payloadName));
        $payload->setRunnerSettings($this->getRunnerSettings(self::COMMAND_CONTEXT_WEB));
        $this->w = new AMQPWorker($payload);
        
        $this->w->connect($connection);
        if (empty($queue))
        {
            $queue = \Yii::$app->params['rabbit']['queue'];
        }
    
        $this->w->listen($queue);
    }

    private function getConnection() : AMQPStreamConnection {
        $rabbitConnectionData = Yii::$app->params['rabbit'];
        return AMQPConnectionBuilder::build(
            $rabbitConnectionData['host'],
            (string)$rabbitConnectionData['port'],
            $rabbitConnectionData['user'],
            $rabbitConnectionData['password'],
            $rabbitConnectionData[AMQPWorker::CONFIG_INDEX_HOST]
        );
    }

    //string $queue, string $messagePath
    public function actionAddMessage() {
        $conn = $this->getConnection();
        $publisher = new AMQPPublisher($conn);
        $channel = $conn->channel();
        $message = $publisher->buildMessage(
            1,
            'test.com',
            commands\PingCommand::class,
            ['previousCommand' => null]
        );
        $publisher->publishMessage($message, AbstractCommand::RABBIT_EXCHANGE_DEFAULT);
        $channel->close();
        $conn->close();
    }
    
    public function __destruct()
    {
        if ($this->w instanceof AMQPWorker)
        {
            $this->w->shutdown();
        }        
    }

    private function getRunnerSettings($cliParam) : ?RunnerSettings {
        switch ($cliParam) {
            case self::COMMAND_CONTEXT_WEB:
                return new WebRunnerSettings(
                    getenv('SERVICE_NAME'),
                    intval(getenv('SERVICE_PORT'))
                );
            case self::COMMAND_CONTEXT_CLI:
                return new CliRunnerSettings();
        }
        throw new \Exception('Unknown runner settings type '.$cliParam);
    }
}
