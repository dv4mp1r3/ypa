const viewer = new BpmnJS({
    container: document.getElementById('bpmn'),
    height: 600
});
$.ajax('/task-workflow.bpmn', { dataType : 'text' }).done(async function(xml) {
    try {
        await viewer.importXML(xml);
        viewer.get('canvas').zoom('fit-viewport');
    } catch (err) {
        console.error(err);
    }
});