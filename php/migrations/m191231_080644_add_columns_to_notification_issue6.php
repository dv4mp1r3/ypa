<?php

use yii\db\Migration;
use app\models\Notification;
use app\models\auth\User;

/**
 * Class m191231_080644_add_columns_to_notification_issue6
 */
class m191231_080644_add_columns_to_notification_issue6 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $db = $this->getDb();
        
        $queries = [
            "ALTER TABLE "
                .Notification::tableName()
                ." MODIFY COLUMN `text` TEXT NULL COMMENT 'текст уведомления'",
            
            "ALTER TABLE "
                .Notification::tableName()
                ." ADD taken_by_user_id  int(10) unsigned NULL COMMENT 
                    'каким пользователем взято уведомление от задачи админа'",
            
            "ALTER TABLE "
                .Notification::tableName()
                ." ADD CONSTRAINT taken_by_user_notifications FOREIGN KEY (taken_by_user_id) REFERENCES "
                .User::tableName()."(id)",
            
            "CREATE INDEX notification_taken_by_user_id_IDX USING BTREE ON "
                .Notification::tableName()
                ." (taken_by_user_id)",
            
            "ALTER TABLE "
                .Notification::tableName()
                ." ADD short_info VARCHAR(255) NULL COMMENT 'краткая информация по уведомлению (то, что видит пользователь)'",
            
        ];
        foreach ($queries as $query)
        {
            $command = $db->createCommand($query);
            $command->execute();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $db = $this->getDb();
        $queries = [
            "ALTER TABLE "
                .Notification::tableName()
                ." MODIFY COLUMN `text` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT 'текст уведомления'",
            
            "DROP INDEX notification_taken_by_user_id_IDX ON "
                .Notification::tableName(),
            
            "ALTER TABLE "
                .Notification::tableName()
                ." DROP FOREIGN KEY taken_by_user_notifications",
            
            "ALTER TABLE "
                .Notification::tableName()
                ." DROP COLUMN taken_by_user_id",
            
            "ALTER TABLE "
                .Notification::tableName()
                ." DROP COLUMN short_info",
        ];
        foreach ($queries as $query)
        {
            $command = $db->createCommand($query);
            $command->execute();
        }     
    }
}
