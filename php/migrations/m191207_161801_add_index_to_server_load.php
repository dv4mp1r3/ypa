<?php

use yii\db\Migration;
use \app\models\server\LoadRecord;
/**
 * Class m191207_161801_add_index_to_server_load
 */
class m191207_161801_add_index_to_server_load extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->getDb()->createCommand("ALTER TABLE `".LoadRecord::tableName()."` 
ADD INDEX `creation_date` (`creation_date`) USING BTREE;")->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->getDb()->createCommand("ALTER TABLE `".LoadRecord::tableName()."`
	DROP INDEX `creation_date`,
");
    }

}
