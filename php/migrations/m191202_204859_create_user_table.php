<?php

use yii\db\Migration;
use app\models\auth\User;

/**
 * Handles the creation of table `user`.
 */
class m191202_204859_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->getDb()->createCommand("CREATE TABLE `".User::tableName()."` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL DEFAULT '' COLLATE 'utf8_bin',
	`password` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8_bin',
	`auth_key` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_bin',
	`token` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_bin',
	`disabled` BIT(1) NOT NULL DEFAULT b'0',
	`is_admin` BIT(1) NOT NULL DEFAULT b'0',
	`creation_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`update_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	`last_login` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `name` (`name`),
	INDEX `id` (`id`),
	INDEX `created_time` (`creation_date`),
	INDEX `updated_time` (`update_date`)
)
COLLATE='utf8_bin'
ENGINE=InnoDB
AUTO_INCREMENT=1
;
")->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(User::tableName());
    }
}
