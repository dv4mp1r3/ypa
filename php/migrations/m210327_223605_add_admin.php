<?php

use yii\db\Migration;

/**
 * Class m210327_223605_add_admin
 */
class m210327_223605_add_admin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
       $u = new \app\models\auth\User();
       $u->name = getenv('YPA_ADMIN_USERNAME');
       $u->password = getenv('YPA_ADMIN_PASSWORD');
       $u->is_admin = true;
       return $u->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $u = \app\models\auth\User::findByUsername(getenv('YPA_ADMIN_USERNAME'));
        return $u->delete();
    }
}
