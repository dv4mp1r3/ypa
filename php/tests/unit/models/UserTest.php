<?php

namespace tests\unit\models;

use app\models\User;
use Codeception\Test\Unit;

class UserTest extends Unit
{
    public function testFindUserById()
    {
        $user = User::findIdentity(100);
        $this->assertTrue($user instanceof User);
        $this->assertEquals('admin', $user->username);

        $this->assertNotTrue(User::findIdentity(999));
    }

    public function testFindUserByAccessToken()
    {
        $user = User::findIdentityByAccessToken('100-token');
        $this->assertTrue($user instanceof User);
        $this->assertEquals('admin', $user->username);

        $this->assertNotTrue(User::findIdentityByAccessToken('non-existing'));        
    }

    public function testFindUserByUsername()
    {
        $user = User::findByUsername('admin');
        $this->assertTrue($user instanceof User);
        $this->assertNotTrue(User::findByUsername('not-admin'));
    }

    /**
     * @depends testFindUserByUsername
     */
    public function testValidateUser($user)
    {
        $user = User::findByUsername('admin');
        $this->assertTrue($user->validateAuthKey('test100key'));
        $this->assertNotTrue($user->validateAuthKey('test102key'));

        $this->assertTrue($user->validatePassword('admin'));
        $this->assertNotTrue($user->validatePassword('123456'));        
    }

}
