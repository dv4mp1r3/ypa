<?php

namespace tests\unit\models;

use app\models\LoginForm;
use Codeception\Test\Unit;

class LoginFormTest extends Unit
{
    private $model;

    protected function _after()
    {
        \Yii::$app->user->logout();
    }

    public function testLoginNoUser()
    {
        $this->model = new LoginForm([
            'username' => 'not_existing_username',
            'password' => 'not_existing_password',
        ]);

        $this->assertNotTrue($this->model->login());
        $this->assertTrue(\Yii::$app->user->isGuest);
    }

    public function testLoginWrongPassword()
    {
        $this->model = new LoginForm([
            'username' => 'demo',
            'password' => 'wrong_password',
        ]);

        $this->assertNotTrue($this->model->login());
        $this->assertTrue(\Yii::$app->user->isGuest);
        $this->assertTrue(isset($this->model->errors['password']));

    }

    public function testLoginCorrect()
    {
        $this->model = new LoginForm([
            'username' => 'admin',
            'password' => 'admin',
        ]);

        $this->assertTrue($this->model->login());
        $this->assertNotTrue(\Yii::$app->user->isGuest);
        $this->assertTrue(!isset($this->model->errors['password']));
    }

}
