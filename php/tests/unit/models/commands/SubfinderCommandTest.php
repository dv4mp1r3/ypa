<?php

namespace app\tests\unit\models\commands;

use app\models\commands\SubfinderCommand;

class SubfinderCommandTest extends BaseCommandTest
{
    /**
     * @var app\models\commands\SubfinderCommand
     */
    protected $subfinderCommand;

    public function getTestOutputFilename(): string
    {
        return 'subfinder.txt';
    }

    public function setUp() : void
    {
        $this->subfinderCommand = new SubfinderCommand();
    }

    public function tearDown() : void
    {
        unset($this->subfinderCommand);
    }

    public function testPreExecute()
    {
        $this->subfinderCommand->domain = 'test.domain';
        $this->subfinderCommand->preExecute();
        $this->assertEquals('subfinder -d test.domain', $this->subfinderCommand->getCommand());
    }

    public function testPostExecute()
    {
        $publisher = new \app\models\AMQPPublisher(self::getAMQPConnection());
        /**
         * @var SubfinderCommand $subfinderCommand
         */
        $subfinderCommand = $this->make(
            get_class($this->subfinderCommand),
            [
                'output' => $this->testOutputData,
                'domain' => 'host.com',
                'taskId' => 1,
            ]
        );
        $subfinderCommand->setPublisher($publisher);
        $subfinderCommand->postExecute();
        $this->assertEquals(4, $subfinderCommand->getFoundDomainsCount());
    }
    
}