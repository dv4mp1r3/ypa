<?php

namespace app\tests\unit\models\commands;

use Codeception\Test\Unit;
use PhpAmqpLib\Connection\AMQPStreamConnection;

abstract class BaseCommandTest extends Unit
{

    protected $testOutputData = '';

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        $testOutputFilename = $this->getTestOutputFilename();
        if (!empty($testOutputFilename))
        {
            $this->testOutputData = file_get_contents($this->getOutputFilenameFullPath($testOutputFilename));
        }
        parent::__construct($name, $data, $dataName);
    }

    /**
     * @param string $filename
     * @return string
     */
    protected function getOutputFilenameFullPath(string $filename) : string
    {
        $fullname = __DIR__.'/../../../data/outputExamples/'.$filename;
        return $fullname;
    }

    public abstract function getTestOutputFilename() : string;

    protected function getAMQPConnection() : AMQPStreamConnection
    {
        $mockedAMQPConnection = $this->make(
            AMQPStreamConnection::class,
            [
                //'__construct' => function($host, $port, $user, $password, $vhost = '/',)
                'connectOnConstruct' => function() {return false;},
                'channel' => function() {return null;}
            ]
        );
        return $mockedAMQPConnection;
    }
}