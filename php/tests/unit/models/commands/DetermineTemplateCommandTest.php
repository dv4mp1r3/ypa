<?php

declare(strict_types=1);

namespace app\tests\unit\models\commands;

use app\models\AMQPPublisher;
use app\models\commands\DetermineTemplateCommand;

class DetermineTemplateCommandTest extends BaseCommandTest
{
    const URL = 'url.com';

    private DetermineTemplateCommand $determineTemplate;

    public function setUp() : void
    {
        $this->determineTemplate = new DetermineTemplateCommand();
        parent::setUp();
    }

    public function tearDown() : void
    {
        unset($this->determineTemplate);
        parent::tearDown();
    }

    public function testPreExecute()
    {
        $this->determineTemplate->url = self::URL;
        $this->determineTemplate->preExecute();
        $this->assertEquals(
            'php yii internal-commands/load-from-url '.self::URL,
            $this->determineTemplate->getCommand()
        );
    }

    public function testParsing()
    {
        $publisher = new AMQPPublisher(self::getAMQPConnection());
        /**
         * @var DetermineTemplateCommand $tmpCommand
         */
        $tmpCommand = $this->make(
            get_class($this->determineTemplate),
            [
                'output' => $this->testOutputData,
            ]
        );
        $tmpCommand->setPublisher($publisher);
        $tmpCommand->url = self::URL;
        $tmpCommand->postExecute();
        $this->assertEquals(1, $tmpCommand->getFoundTemplates());
    }

    public function getTestOutputFilename(): string
    {
        return 'determineTemplate.txt';
    }
}