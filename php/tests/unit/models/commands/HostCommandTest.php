<?php

namespace app\tests\unit\models\commands;

use app\models\commands\HostCommand;

class HostCommandTest extends BaseCommandTest
{
    /**
     * @var HostCommand
     */
    protected $hostCommand;

    public function setUp() : void
    {
        $this->hostCommand = new HostCommand();
        parent::setUp();
    }

    public function tearDown() : void
    {
        unset($this->hostCommand);
        parent::tearDown();
    }

    public function getTestOutputFilename() : string
    {
        return 'host.txt';
    }

    public function testSetCommand()
    {
        $command = 'test';
        $this->hostCommand->setCommand($command);
        $this->assertEquals('test', $this->hostCommand->getCommand());
    }

    public function testGetCommand()
    {
        $command = 'test';
        $this->hostCommand->setCommand($command);
        $this->assertEquals('test', $this->hostCommand->getCommand());
    }

    public function testPreExecute()
    {
        $this->hostCommand->domain = 'test.domain';
        $this->hostCommand->preExecute();
        $this->assertEquals('host test.domain', $this->hostCommand->getCommand());
    }


    public function testHostParsing()
    {
        $publisher = new \app\models\AMQPPublisher(self::getAMQPConnection());
        /**
         * @var HostCommand $tmpHostCommand
         */
        $tmpHostCommand = $this->make(
            get_class($this->hostCommand),
            [
                'output' => $this->testOutputData,
                'domain' => 'localhost',
                'taskId' => 1,
            ]
        );
        $tmpHostCommand->setPublisher($publisher);
        $tmpHostCommand->postExecute();
        $publishedMessages = $tmpHostCommand->getPublishedMessages();

        $messageExpected = [
            'taskId' => 1,
            'domain' => 'localhost',
            'command' => 'app\\models\\commands\\NmapCommand',
            'extra' => [
                'host' => '127.0.0.1',
            ],
        ];
        $data = json_decode($publishedMessages[0], true);
        $this->assertEquals($messageExpected, $data);
        $this->assertEquals(1,$tmpHostCommand->getPublishedAddressesCount());
    }
}