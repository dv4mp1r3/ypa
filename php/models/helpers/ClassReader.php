<?php

declare(strict_types=1);

namespace app\models\helpers;

use Yii;

class ClassReader
{
    const NAMESPACE_ROOT = 'app';

    private string $nameSpace;

    private string $fullPath;

    public function __construct(string $nameSpace)
    {
        $this->nameSpace = $nameSpace;
        return $this;
    }

    public function getClassNames($ignoreInterfaces = true): array
    {
        $this->buildFullPath();
        $files = $this->getFiles(
            $this->fullPath,
            $ignoreInterfaces
        );
        foreach ($files as &$file) {
            $file = str_replace('.php', '', $file);
        }
        return $files;
    }

    private function buildFullPath() : void
    {
        $rootPath = Yii::getAlias('@'.self::NAMESPACE_ROOT);
        $directory = $this->filterNamespace($this->nameSpace);
        $this->fullPath = $rootPath.DIRECTORY_SEPARATOR.$directory;
    }

    private function filterNamespace(string $nameSpace) : string
    {
        $pos = mb_strpos($nameSpace, self::NAMESPACE_ROOT.'\\');
        if ($pos !== false) {
            return str_replace(
                '\\',
                DIRECTORY_SEPARATOR,
                mb_substr(
                    $nameSpace,
                    $pos + mb_strlen(self::NAMESPACE_ROOT) + 1
                )
            );
        }
        return $nameSpace;
    }

    private function getFiles($dir, $ignoreInterfaces) : array {
        $files = scandir($dir);
        if ($files === false) {
            return [];
        }

        $exclude = ['.', '..'];
        return array_filter(
            $files,
            //todo: redid
            function ($el) use ($exclude, $ignoreInterfaces) {
                $res = !in_array($el, $exclude);
                if ($ignoreInterfaces) {
                    $substr = mb_substr($el, 0, 2);
                    $res = $res && !($substr[0] === 'I' && ctype_upper($substr));
                }
                return $res;
            }
        );
    }

    public function getFullPath() : string
    {
        return $this->fullPath;
    }
}