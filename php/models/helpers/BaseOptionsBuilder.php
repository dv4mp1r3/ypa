<?php

declare(strict_types=1);

namespace app\models\helpers;

use yii\helpers\Json;

class BaseOptionsBuilder
{
    protected array $optionsTemplate = [];

    public function updateOptions(array $options): BaseOptionsBuilder {
        $this->optionsTemplate = $options;
        return $this;
    }

    public function getOptionsAsJson(int $flags = JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE): string {
        return Json::encode($this->getOptions(), $flags);
    }

    public function getOptions(): array {
        return $this->optionsTemplate;
    }
}