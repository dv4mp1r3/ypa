<?php

declare(strict_types=1);

namespace app\models\helpers;

use Yii;

/**
 * Хелпер для генерации прав доступа к действиям в зависимости от того, включен ли в окружении демо режим или нет
 */
class DemoRulesHelper
{
    private array $availActions;

    private array $nonDemoActions;

    private array $allActions;

    /**
     * @param array $availActions список действий, которые должны быть доступны всегда
     * @param array $nonDemoActions список действий, которые не должны быть доступны в демо-режиме
     */
    public function __construct(array $availActions, array $nonDemoActions)
    {
        $this->availActions = $availActions;
        $this->nonDemoActions = $nonDemoActions;
        $this->allActions = array_merge($availActions, $nonDemoActions);
    }

    public function getAllActions() : array
    {
        return $this->allActions;
    }

    public function getRules() : array
    {
        $rules = [];
        if (Yii::$app->params['isDemo'] === 0) {
            $rules[] = [
                'actions' => $this->allActions,
                'allow' => true,
                'roles' => ['@'],
            ];
        }
        else {
            $rules[] = [
                'actions' => $this->availActions,
                'allow' => true,
                'roles' => ['@'],
            ];
            $rules[] = [
                'actions' => $this->nonDemoActions,
                'allow' => false,
                'roles' => ['@'],
            ];
        }
        return $rules;
    }
}