<?php

declare(strict_types=1);

namespace app\models\helpers;

use yii\base\Model;
use \ReflectionClass;

class FieldHelper
{
    /**
     * Получение имени числового поля по названию этого поля и значению
     * (происходит поиск имен констант по шаблону и генерация строкового значения)
     * Например, при использовании свойства модели type и имени константы TYPE_NAME будет возвращена строка "name"
     * @param Model $m
     * @param string $fieldName
     * @param int $fieldValue
     * @return string
     */
    public static function getTypeString(Model $m, string $fieldName, int $fieldValue) : string
    {
        $capitalizedFieldName = strtoupper($fieldName);
        $cs = self::getConstantsByField($m, $capitalizedFieldName, $fieldValue);

        return is_array($cs) && count($cs) === 1
            ? strtolower(str_replace("{$capitalizedFieldName}_", "", array_key_first($cs)))
            : '';
    }

    public static function getTypeValueByString(Model $m, string $constantType, string $constantName) : int
    {
        $constantName = strtoupper($constantType)."_".strtoupper($constantName);
        $fooClass = new ReflectionClass($m);
        $constants = $fooClass->getConstants();
        return array_key_exists($constantName, $constants) ? $constants[$constantName] : -1;
    }

    public static function getFieldValues(Model $m, string $fieldName, $valuesAreKeys = true, $useUcfirst = false) : array
    {
        $capitalizedFieldName = mb_strtoupper($fieldName);
        $constants = self::getFieldConstants($m, $capitalizedFieldName);
        $result = [];
        foreach ($constants as $key => $value) {
            $resultKey = mb_strtolower(str_replace("{$capitalizedFieldName}_", '', $key));
            if ($useUcfirst) {
                $resultKey = ucfirst($resultKey);
            }

            if ($valuesAreKeys) {
                $result[$value] = $resultKey;
            } else {
                $result[$resultKey] = $value;
            }
        }
        return $result;
    }

    public static function getFieldConstants(Model $m, string $fieldName) : array
    {
        $fieldName = strtoupper($fieldName);
        $fooClass = new ReflectionClass($m);
        return array_filter(
            $fooClass->getConstants(),
            function($value, $name) use ($fieldName) {
                return strpos($name, $fieldName) === 0;
            } ,
            ARRAY_FILTER_USE_BOTH
        );
    }

    private static function getConstantsByField(Model $n, string $field, int $fieldVal) : array
    {
        $fooClass = new ReflectionClass($n);
        return array_filter(
            $fooClass->getConstants(),
            function($value, $name) use ($field, $fieldVal) {
                return strpos($name, $field) === 0 && $value === $fieldVal;
            } ,
            ARRAY_FILTER_USE_BOTH
        );
    }

    /**
     * Форматирование результата, полученного вызовом getTypeString в строку для отображения в таблицах/полях/отчетах
     * Пример: directory_traversal -> Directory traversal
     * @param string $value
     * @param bool $ucfirst
     * @return string
     */
    public static function beautify(string $value, bool $ucfirst = false) : string
    {
        $str = str_replace('_', ' ', $value);
        if ($ucfirst) {
            return ucfirst($str);
        }
        return $str;
    }
}