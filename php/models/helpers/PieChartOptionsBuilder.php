<?php

declare(strict_types=1);

namespace app\models\helpers;

class PieChartOptionsBuilder extends BaseOptionsBuilder
{
    protected array $optionsTemplate = [
        'maintainAspectRatio' => false,
        'tooltips' => [
            'backgroundColor' => 'rgb(255,255,255)',
            'bodyFontColor' => '#858796',
            'borderColor' => '#dddfeb',
            'borderWidth' => 1,
            'xPadding' => 15,
            'yPadding' => 15,
            'displayColors' => false,
            'caretPadding' => 10,
        ],
        'legend' => [
            'display' => true,
        ],
        'cutoutPercentage' => 80,
    ];

}