<?php


namespace app\models\helpers;


use yii\web\JsExpression;
use yii\helpers\Json;


class AreaChartOptionsBuilder extends BaseOptionsBuilder
{

    const DEFAULT_TOOLTIP_LABEL_CALLBACK = 'function (tooltipItem, chart) {return chart.datasets[tooltipItem.datasetIndex].label || "";}';
    const DEFAULT_Y_AXES_FORMAT = 'function (value, index, values) {return value;}';

    protected array $optionsTemplate = [
        'maintainAspectRatio' => false,
        'layout' => [
            'padding' => [
                'left' => 10,
                'right' => 25,
                'top' => 25,
                'bottom' => 0,
            ],
        ],
        'scales' => [
            'xAxes' => [[
                'time' => [
                    'unit' => 'date',
                ],
                'gridLines' => [
                    'display' => false,
                    'drawBorder' => false,
                ],
                'ticks' => [
                    'maxTicksLimit' => 7,
                ],
            ],],
            'yAxes' => [[
                'ticks' => [
                    'maxTicksLimit' => 5,
                    'padding' => 10,
                    'callback' => [],
                ],
                'gridLines' => [
                    'color' => 'rgb(234, 236, 244)',
                    'zeroLineColor' => 'rgb(234, 236, 244)',
                    'drawBorder' => false,
                    'borderDash' => [2,],
                    'zeroLineBorderDash' => [2,],
                ],
            ],],
        ],
        'legend' => [
            'display' => false,
        ],
        'tooltips' => [
            'backgroundColor' => 'rgb(255,255,255)',
            'bodyFontColor' => '#858796',
            'titleMarginBottom' => 10,
            'titleFontColor' => '#6e707e',
            'titleFontSize' => 14,
            'borderColor' => '#dddfeb',
            'borderWidth' => 1,
            'xPadding' => 15,
            'yPadding' => 15,
            'displayColors' => false,
            'intersect' => false,
            'mode' => 'index',
            'caretPadding' => 10,
            'callbacks' => [
                'label' => [],
            ],
        ],
    ];

    public function setYAxesValueFormat(JsExpression $exp): AreaChartOptionsBuilder  {
        $this->optionsTemplate['scales']['yAxes']['ticks']['callback'] = $exp;
        return $this;
    }

    public function setTooltipLabelCallback(JsExpression $exp): AreaChartOptionsBuilder {
        $this->optionsTemplate['tooltips']['callbacks']['label'] = $exp;
        return $this;
    }
}