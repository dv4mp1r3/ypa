<?php
declare(strict_types=1);

namespace app\models\runners;

use app\models\commands\AbstractCommand;

abstract class AbstractRunner implements Runnable
{
    protected $command;

    public abstract function run();

    public abstract function getResult() : string;

    public function setCommand(AbstractCommand $command) {
        $this->command = $command;
    }
}