<?php
declare(strict_types=1);

namespace app\models\runners;

use app\models\commands\WebServiceCommand;

class WebRunnerSettings implements RunnerSettings
{
    private $serviceName;

    private $servicePort;

    public function __construct(string $serviceName, int $servicePort)
    {
        $this->serviceName = $serviceName;
        $this->servicePort = $servicePort;
    }

    public function initSettings(Runnable $runnable)
    {
        $runnable->setServiceName($this->serviceName)
            ->setServicePort($this->servicePort);
    }

    public function createRunner(): Runnable
    {
        return new WebServiceRunner();
    }
}