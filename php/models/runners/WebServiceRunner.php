<?php
declare(strict_types=1);

namespace app\models\runners;

use app\models\commands\WebServiceCommand;
use app\exceptions\ValueError;

/**
 * Class WebServiceRunner
 * @property WebServiceCommand command
 * @package app\models\runners
 */
class WebServiceRunner extends AbstractRunner
{
    private $serviceName;

    private $port;

    private $result;

    public function run()
    {
        $ch = curl_init();
        $url = "http://{$this->serviceName}:{$this->port}/".$this->command->toWebServiceRequest();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $this->result = curl_exec($ch);
        if (curl_errno($ch) !== CURLE_OK) {
            throw new ValueError(__CLASS__.'->'.__FUNCTION__.':'.curl_error());
        }
        curl_close($ch);
    }

    public function getResult() : string {
        return $this->result;
    }

    public function setServiceName(string $serviceName) : WebServiceRunner {
        $this->serviceName = $serviceName;
        return $this;
    }

    public function setServicePort(int $servicePort) : WebServiceRunner {
        $this->port = $servicePort;
        return $this;
    }
}