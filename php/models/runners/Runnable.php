<?php
declare(strict_types=1);

namespace app\models\runners;

interface Runnable
{
    public function run();
}