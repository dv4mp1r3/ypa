<?php


namespace app\models\runners;


use app\models\commands\AbstractCommand;

/**
 * Class CliRunner
 * @property AbstractCommand $command
 * @package app\models\runners
 */
class CliRunner extends AbstractRunner
{
    private $result;

    public function run()
    {
        $this->result = shell_exec($this->command->getCommand());
    }

    public function getResult(): string
    {
        return $this->result;
    }

}