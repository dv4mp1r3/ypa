<?php


namespace app\models\runners;


interface RunnerSettings
{
    public function initSettings(Runnable $runnable);

    public function createRunner() : Runnable;
}