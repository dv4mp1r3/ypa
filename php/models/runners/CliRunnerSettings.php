<?php
declare(strict_types=1);

namespace app\models\runners;


class CliRunnerSettings implements RunnerSettings
{

    public function initSettings(Runnable $runnable)
    {

    }

    public function createRunner(): Runnable
    {
        return new CliRunner();
    }
}