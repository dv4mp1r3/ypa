<?php

declare(strict_types=1);

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\auth\User;
use yii\web\IdentityInterface;
use app\exceptions\AccessException;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = null;


    /**
     * @return array the validation rules.
     */
    public function rules(): array
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login(): bool
    {
        if ($this->validate()) {
            $user = $this->getUser();
            $result = Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0);
            if (!($user instanceof User)) {
                return $result;
            }
            if (!$this->updateLastLogin($user)) {
                Yii::$app->getLog()->dispatch($user->getErrors());
            }
            return $result;
        }
        return false;
    }

    /**
     * @param User $user
     * @return boolean
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    protected function updateLastLogin(User $user): bool
    {
        $user->last_login = date_create('now', new \DateTimeZone(\Yii::$app->timeZone))->format('Y-m-d H:i:s');
        $result = $user->update(true, ['last_login']);
        return $result === 1;
    }
    
    public function loginAs(int $userId) : bool
    {
        $currentUser = $this->getIdentity();
        // если текущий пользователь не админ или в сессии не было идентификатора админа,
        // который ранее выполнял вход в учетку
        if ($currentUser instanceof User && !$currentUser->isAdmin())
        {
            throw new AccessException('Only administrator can login as other user');
        }
        $user = User::findIdentity($userId);
        if (!($user instanceof \yii\web\IdentityInterface))
        {
            return false;
        }
        // если нужно вернуться в учетку админа после логаута, чистим ключ админа из сессии
        $result = Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0);
        return $result;
    }
    /**
     * Finds user by [[username]]
     *
     * @return \yii\web\IdentityInterface|null
     */
    public function getUser()
    {    
        if ($this->_user === null) {
            $identity = $this->getIdentity();
            $this->_user = $identity::findByUsername($this->username);
        }

        return $this->_user;
    }

    private function getIdentity(): IdentityInterface
    {
        $identity = Yii::$app->user->getIdentity();
        if ($identity === null) {
            $class = Yii::$app->user->identityClass;
            return new $class();
        }
        return $identity;
    }
}
