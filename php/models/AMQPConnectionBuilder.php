<?php

namespace app\models;

use PhpAmqpLib\Connection\AMQPStreamConnection;


class AMQPConnectionBuilder
{
    const DEFAULT_CONNECTION_TIMEOUT = 60 * 50;
    const DEFAULT_READ_WRITE_TIMEOUT = 60 * 50;
    const DEFAULT_HEARTBEAT = 30 * 50;

    const LOGIN_METHOD = 'AMQPLAIN';
    const LOCALE = 'en_US';

    public static function build(
        string $host,
        string $port,
        string $user,
        string $password,
        string $vhost) : AMQPStreamConnection
    {
        return new AMQPStreamConnection(
            $host,
            $port,
            $user,
            $password,
            $vhost,
            false,
            self::LOGIN_METHOD,
            null,
            self::LOCALE,
            self::DEFAULT_CONNECTION_TIMEOUT,
            self::DEFAULT_READ_WRITE_TIMEOUT,
            null,
            false,
            self::DEFAULT_HEARTBEAT);
    }
}