<?php

declare(strict_types=1);

namespace app\models; 

use \app\models\helpers\HttpResource;

class PageSource
{
    const CHARS_TO_REMOVE = [' ', "\n", "\t", "\r"];

    protected string $sourceUrl;
    
    protected string $sourceContent;
    
    /**
     * 
     * @param string $url
     */
    public function __construct(string $url)
    {
        $this->sourceUrl = $url;
       
    }

    /**
     * загрузка ресурса по урлу через curl
     * @param array|null $curlOptions ассоциативный массив для курла
     * @see HttpResource::get
     */
    public function load(array $curlOptions = [])
    {
        $this->sourceContent = HttpResource::get($this->sourceUrl, $curlOptions);
    }
    
    /**
     * 
     * @return string
     */
    public function getContent()
    {
        return $this->sourceContent;
    }

    /**
     * проверка на то что подстрока есть в загруженной странице
     * @param string $sourceContent
     * @param string $substr
     * @return boolean
     */
    public static function sourceContains(string $sourceContent, string $substr): bool
    {
        return strpos(
            self::filterString($sourceContent),
            self::filterString($substr)) !== false;
    }

    private static function filterString(string $string): string
    {
        return str_replace(
            PageSource::CHARS_TO_REMOVE,
            '', $string
        );
    }
}
