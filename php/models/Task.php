<?php
namespace app\models;

use Yii;
use yii\web\UploadedFile;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use app\models\auth\User;
use app\exceptions\UploadFileException;
use app\exceptions\SaveModelException;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property string $name
 * @property integer $id_parent_task
 * @property string $creation_date
 * @property string $filename
 * @property string $status
 * @property integer $user_id
 */
class Task extends \yii\db\ActiveRecord
{
    
    const STATUS_CREATED = 'created';
    const STATUS_APPROVED = 'approved';

    const UPLOAD_FOLDER = 'uploads';

    public $domains;

    /**
     * @inheritdoc
     */
    public static function tableName() : string
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules() : array
    {
        return [
            [['name', 'filename'], 'required'],
            [['id_parent_task', 'user_id'], 'integer'],
            [['creation_date'], 'safe'],
            [['name', 'filename', 'status'], 'string', 'max' => 255],
            [['filename'], 'unique'],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() : array
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            //'id_parent_task' => 'Id Parent Task',
            //'creation_date' => 'Creation Date',
            //'filename' => 'Filename',
        ];
    }

    /**
     * @param yii\web\UploadedFile $uf 
     * @return string
     */
    public function uploadFile(UploadedFile $uf) : string
    {
        if (!($uf instanceof UploadedFile)) {
            throw new UploadFileException('$uf is not instance of UploadedFile');
        }

        $filePath = $this->generateTaskFilePath($uf->getBaseName());
        if (!$uf->saveAs($filePath)) {
            throw new UploadFileException("Error on save UploadedFile (code {$uf->error})");
        }
        return $filePath;
    }

    public function generateTaskFilePath(string $baseName, string $alias = '@webroot') : string
    {
        $basename = stripslashes($baseName) . '-' . md5(time());
        $ext = 'txt';
        $filePath = Yii::getAlias($alias) . "/../" . Task::UPLOAD_FOLDER;
        $filePath = str_replace(['web/../', '/../'], ['', '/'], $filePath);
        if (!file_exists($filePath)) {
            mkdir($filePath, 0755);
        }
        $filePath .= "/$basename.$ext";
        return $filePath;
    }

    /**
     * 
     * @return array
     */
    public function readDomains() : array
    {
        $content = file_get_contents($this->filename);
        $lines = explode("\n", str_replace("\r", '', $content));
        $lines = array_unique($lines);
        foreach ($lines as $key => &$line) {
            if (empty($line)) {
                unset($lines[$key]);
            }
        }
        return $lines;
    }

    /**
     * @param boolean $runValidation
     * @param array $attributeNames
     * @return bool
     * @throws \Exception
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $this->creation_date = $this->getCreationDate();
        $userId = User::getCurrentUserId();
        $this->user_id = $userId > 0 ? $userId : null;
        $saveResult = parent::save();
        if (!$saveResult) {
            throw new SaveModelException($this->getErrorsAsString());
        }
        return $saveResult;
    }

    private function getCreationDate() : string
    {
        return date_create('now', new \DateTimeZone(\Yii::$app->timeZone))
            ->format('Y-m-d H:i:s');
    }
    
    public function getErrorsAsString() : string
    {
        $string = '';
        $errors = $this->getErrors();
        foreach ($errors as $attributeName => &$attributeErrors) {
            $string .= "\nAttribute - '$attributeName':\n" . implode("\n", $attributeErrors);
        }
        return $string;
    }
    
    /**
     * 
     * @param string $queue
     * @return integer 
     */
    public function pushToQueue(string $queue) : int
    {
        $rabbitConnectionData = Yii::$app->params['rabbit'];
  
        $exchange = commands\AbstractCommand::RABBIT_EXCHANGE_DEFAULT;
        $connection = new AMQPStreamConnection(
            $rabbitConnectionData['host'], 
            $rabbitConnectionData['port'], 
            $rabbitConnectionData['user'], 
            $rabbitConnectionData['password'],
            $rabbitConnectionData['vhost']);
        $channel = $connection->channel();
        $publisher = new AMQPPublisher($connection);
        $domainsCount = 0;
        foreach ($this->readDomains() as $domain)
        { 
            $message = $publisher->buildMessage(
                $this->id,
                $domain,
                commands\PingCommand::class,
                ['previousCommand' => null]
            );
            $publisher->publishMessage($message, $exchange, AMQPPublisher::ROUTING_KEY_HOST_ADDED);
            $domainsCount++;
        }
        
        $channel->close();
        $connection->close();
        
        return $domainsCount;
    }
    
    /**
     * 
     * @param string $domain
     * @param string $commandName
     * @param array $extra
     * @return AMQPMessage
     */
    protected function buildRabbitMessage(string $domain, string $commandName, array $extra = null) : AMQPMessage
    {
        $msg = [
            'taskId' => $this->id,
            'domain' => $domain,
            'command' => $commandName === null ? 'host' : $commandName,
            'extra' => $extra,
        ];
        
        return new AMQPMessage(json_encode($msg),
            [
                'content_type' => 'text/plain',
                'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT
            ]);
    }
    
    public function getUser() : ActiveQueryInterface
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
