<?php

declare(strict_types=1);

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use app\exceptions\ValueError;

/**
 * This is the model class for table "notification".
 *
 * @property integer $id
 * @property string $text
 * @property string $creation_date
 * @property string $extra
 * @property integer $task_id
 * @property string $command
 * @property integer $type
 * @property integer $level
 * @property integer $taken_by_user_id
 * @property string $short_info
 *
 * @property Task $task
 */
class Notification extends \yii\db\ActiveRecord
{
    const LEVEL_UNKNOWN = 0;
    const LEVEL_LOW = 1;
    const LEVEL_MEDIUM = 2;
    const LEVEL_HIGH = 3;
    const LEVEL_CRITICAL = 4;

    const TYPE_UNKNOWN = 0;
    const TYPE_DOS = 1;
    const TYPE_CODE_EXECUTION = 2;
    const TYPE_OVERFLOW = 3;
    const TYPE_MEMORY_CORRUPTION = 4;
    const TYPE_SQL_INJECTION = 5;
    const TYPE_XSS = 6;
    const TYPE_DIRECTORY_TRAVERSAL = 7;
    const TYPE_RESPONSE_SPLITTING = 8;
    const TYPE_BYPASS = 9;
    const TYPE_GAIN_INFORMATION = 10;
    const TYPE_GAIN_PRIVILEGES = 11;
    const TYPE_CSRF = 12;
    const TYPE_FILE_INCLUSION = 13;

    public static function getLevelsAsArray() : array
    {
        return [
            'Unknown',
            'Low',
            'Medium',
            'High',
            'Critical',
        ];
    }

    public static function getTypesAsArray(): array
    {
        return [
            'DOS',
            'Code Execution',
            'Overflow',
            'Memory corruption',
            'SQL injection',
            'XSS',
            'Directory traversal',
            'Response splitting',
            'Bypass',
            'Gain information',
            'Gain privileges',
            'CSRF',
            'File inclusion',
        ];
    }


    /**
     * @inheritdoc
     */
    public static function tableName() : string
    {
        return 'notification';
    }

    /**
     * @inheritdoc
     */
    public function rules() : array
    {
        return [
            [['creation_date'], 'safe'],
            [['extra'], 'string'],
            [['task_id'], 'required'],
            [['task_id', 'type', 'level', 'taken_by_user_id'], 'integer'],
            [['short_info'], 'string', 'max' => 255],
            [['text'], 'string', 'max' => 4000],
            [['command'], 'string', 'max' => 512],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() : array
    {
        return [
            'id' => 'ID',
            //'text' => 'текст уведомления',
            'text' => 'Text',
            'creation_date' => 'Creation Date',
            //'extra' => 'дополнительное поле для помещения кастомных данных, которые возможно понадобятся позже',
            'extra' => 'Extra data',
            //'task_id' => 'связь с задачей',
            'task_id' => 'Task',
            //'command' => 'Оригинальная команда, которая привела к выбросу уведомления',
            'command' => 'Original command',
            //'type' => 'тип уведомления (бага, RCE и прочее)',
            'type' => 'Type',
            //'level' => 'уровень уведомления (некритичный, критический и прочее)',
            'level' => 'Level',
            'taken_by_user_id' => 'Взят пользователем',
            //'short_info' => 'Краткая информация',
            'short_info' => 'Short info',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask() : ActiveQuery
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }
    
    public function getErrorsAsString() : string
    {
        $string = '';
        $errors = $this->getErrors();
        foreach ($errors as $attributeName => &$attributeErrors) {
            $string .= "\nAttribute - '$attributeName':\n" . implode("\n", $attributeErrors);
        }
        return $string;
    }
    
    /**
     * Создано ли уведомление задачей администратора
     * @return bool
     */
    public function isCreatedByAdmin() : bool
    {
        return intval(Task::find()
            ->select(['user.id'])
            ->leftJoin('user', 'user.id = task.user_id')
            ->where(['task.id' => $this->task_id])->count()) === 1;       
    }

    public static function getLevelByValue(int $val): string
    {
        $levelMapping = [
            'unknown',
            'low',
            'medium',
            'high',
            'critical'
        ];
        if (!array_key_exists($val, $levelMapping)) {
            throw new ValueError('Unknown type');
        }
        return $levelMapping[$val];
    }
}
