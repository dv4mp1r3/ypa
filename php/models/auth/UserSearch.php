<?php

declare (strict_types=1);

namespace app\models\auth;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\auth\User;

class UserSearch extends User {
    
    /**
     * @inheritdoc
     */
    public function rules() : array
    {
        return [
            [['id',], 'integer'],
            [[ 'creation_date', 'last_login'], 'safe'],
            [[ 'name'], 'string'],
            [[ 'disabled', 'is_admin'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() : array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params) : ActiveDataProvider
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'creation_date' => $this->creation_date,
            'last_login' => $this->last_login,
            'disabled' => $this->disabled,
            'is_admin' => $this->is_admin,
            
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
    
    
}
