<?php

namespace app\models\auth;
use yii\filters\AccessControl;

class AccessControlAdminOnly extends AccessControl {
    
    /**
     * 
     * @param Action $action
     * @return bool
     */
    public function beforeAction($action) 
    {
        $result = User::currentUserIsAdmin();
        if (!$result) {
            Yii::$app->response->redirect('site/login');
        }
        return $result;
    }
}
