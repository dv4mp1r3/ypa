<?php

declare(strict_types=1);

namespace app\models\commands;

use app\models\server\Process;

/**
 * Class PsCommand
 * @package app\models\commands
 * @deprecated
 */
class PsCommand extends AbstractCommand
{
    const INDEX_USER = 0;
    const INDEX_PID = 1;
    const INDEX_CPU_PERCENT = 2;
    const INDEX_MEMORY_PERCENT = 3;
    const INDEX_COMMAND = 10;

    const MAX_COLUMNS = 11;

    protected $processData = [];

    public function preExecute()
    {
        $this->setCommand("ps aux");
    }

    public function postExecute()
    {
        $lines = explode("\n", $this->output);
        $i = 0;
        foreach ($lines as &$line) {
            $i++;
            if ($i === 1) {
                continue;
            }
            $this->addProcess(new Process($line));
        }
    }

    public function getRunningProcesses() : array
    {
        return $this->processData;
    }

    protected function addProcess(Process $p)
    {
        $this->processData[] = $p;
    }

    public static function getCommandName(): string
    {
        return "ps";
    }

    public function toWebServiceRequest(): string
    {
        return '';
    }
}