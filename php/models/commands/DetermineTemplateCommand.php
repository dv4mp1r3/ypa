<?php

declare(strict_types=1);

namespace app\models\commands;

use app\models\helpers\ClassReader;
use app\models\templates\ISimilar;
use app\models\PageSource;

class DetermineTemplateCommand extends AbstractCommand
{
    const TEMPLATES_NAMESPACE = 'app\\models\\templates';

    public string $url;

    private int $templatesFound = 0;

    public static function getCommandName(): string
    {
        return 'determineTemplate';
    }

    public function preExecute()
    {
        $this->setCommand('php yii internal-commands/load-from-url '.$this->url);
    }

    public function postExecute()
    {
        if (empty($this->output)) {
            return;
        }

        $cl = new ClassReader(self::TEMPLATES_NAMESPACE);
        $classes = $cl->getClassNames();

        foreach ($classes as $class) {
            $class = self::TEMPLATES_NAMESPACE."\\$class";
            /**
             * @var $instance ISimilar
             */
            $template = new $class();
            if (!($template instanceof ISimilar)) {
                continue;
            }

            if ($template->looksLike($this->output)) {
                //todo: push notify
                $this->templatesFound++;
            }
        }

    }

    public function getFoundTemplates(): int
    {
        return $this->templatesFound;
    }

    public function toWebServiceRequest(): string
    {
        return "determine-template/$this->domain";
    }
}