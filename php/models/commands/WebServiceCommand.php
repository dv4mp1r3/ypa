<?php
declare(strict_types=1);

namespace app\models\commands;


interface WebServiceCommand
{
    public function toWebServiceRequest() : string;
}