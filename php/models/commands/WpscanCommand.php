<?php

declare(strict_types=1);

namespace app\models\commands;

use app\models\Notification;

class WpscanCommand extends AbstractCommand
{
    const CODE_VULNERABILITY = '^[[31m[!]^[[0m';
    const CODE_WARNING = '^[[33m[!]^[[0m';
    const CODE_NOTICE = '^[[31m[!]^[[0m';
    
    /**
     *
     * @var string 
     */
    public $domain;
    
    public function postExecute()
    {
        $lines = explode("\n", $this->output);
        $noticeData = [];
        foreach ($lines as &$line) {
            if ($this->lineBeginsAt($line, self::CODE_VULNERABILITY))
            {
                $noticeStr = str_replace(self::CODE_VULNERABILITY, '', $line); 
                $this->saveNotificationToDB(
                    Notification::TYPE_UNKNOWN,
                    Notification::LEVEL_UNKNOWN,
                    $noticeStr
                );
            }
            else if ($this->lineBeginsAt($line, self::CODE_WARNING))
            {
                $noticeData[] = str_replace(self::CODE_WARNING, '', $line);
            }            
        }        
        if (count($noticeData) > 0)
        {
            $this->saveNotificationToDB(
                Notification::TYPE_UNKNOWN,
                Notification::LEVEL_UNKNOWN,
                ['data' => $noticeData]
            );
        }
    }

    public function preExecute()
    {
        $this->setCommand("wpscan --url {$this->domain} --follow-redirection");
    }

    public static function getCommandName() : string
    {
        return 'wpscan';
    }

    public function toWebServiceRequest(): string
    {
        return self::getCommandName()."/{$this->domain}/";
    }
}
