<?php

namespace app\models\commands;

/**
 * @deprecated
 */
class DirParserCommand extends AbstractCommand
{
    /**
     * @var string
     */
    public $domain;

    /**
     * @var string
     */
    public $dictName;

    /**
     * @var string
     */
    public $protocol;

    public function preExecute()
    {
        $this->setCommand(DirParserCommand::getCommandName()." {$this->domain} {$this->dictName}");
    }

    /**
     *
     */
    public function postExecute()
    {
        $result_array = [];
        $i = 0;
        $searchString = "==> DIRECTORY: $this->domain";
        $linestoexplode = str_replace("\r", "\n", $this->output);
        $lines = explode("\n", $linestoexplode);
        foreach ($lines as $line) {
            $occurances_string = substr_count($line, $searchString);
            if ($occurances_string > 0) {
                $fine_line = str_replace("==> DIRECTORY: ", "", $line);
                $result_array[$i] = $fine_line;
                $i++;
            }
        }
        //$this->addAdminsCommand($result_array);
    }

    protected function addAdminsCommand($directory_list)
    {
        $extra = [];
        if (!empty($directory_list)) {
            $extra['directory_list'] = $directory_list;
        }

        $message = $this->buildMessage(
            $this->taskId,
            $this->domain,
            //todo: implement next command
            '',
            $extra
        );
        $this->publishMessage($message, self::RABBIT_EXCHANGE_DEFAULT);
    }

    protected function addServiceDirCommand()
    {
        $extra = [];
        if (!empty($directory_list)) {
            $extra['directory_list'] = $directory_list;
        }

        //todo: refactor this part
        $message = $this->buildMessage(
            $this->taskId,
            $this->domain,
            //todo: implement next command
            '',
            $extra
        );
        $this->publishMessage($message, self::RABBIT_EXCHANGE_DEFAULT);
    }

    public static function getCommandName(): string
    {
        return 'dirb';
    }

    public function toWebServiceRequest(): string
    {
        return DirParserCommand::getCommandName()."/{$this->protocol}/{$this->domain}/{$this->dictName}";
    }

    public function initParameters(\stdClass $msgBody)
    {
        parent::initParameters($msgBody);
        $this->dictName = $msgBody->extra->dictName;
        $this->protocol = $msgBody->extra->protocol;
    }
}