<?php

declare(strict_types=1);

namespace app\models\commands;

class WhoisCommand extends AbstractCommand
{

    public function postExecute()
    {
        
    }

    public function preExecute()
    {
        
    }
    
    public static function getCommandName() : string
    {
        return 'whois';
    }

    public function toWebServiceRequest(): string
    {
        return self::getCommandName()."/{$this->domain}/";
    }
}
