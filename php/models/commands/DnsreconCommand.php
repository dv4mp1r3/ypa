<?php

declare(strict_types=1);

namespace app\models\commands;

use Yii;

class DnsreconCommand extends AbstractCommand
{

    public static function getCommandName()
    {
        return 'dnsrecon';
    }

    public function preExecute()
    {
        $this->setCommand("dnsrecon $this->host");
    }

    public function postExecute()
    {
        $records = json_decode($this->output, true);
        if (json_last_error() > 0) {
            Yii::error(get_class($this)." json_decode error: ".json_last_error());
            Yii::debug(json_last_error_msg());
            return;
        }

        foreach ($records as $record) {
            if (!array_key_exists('address', $record)) {
                continue;
            }
            $domain = $this->getRecordDomain($record);
            $message = $this->buildMessage(
                $this->taskId,
                $domain,
                NmapCommand::class,
                ['host' => $record['address']]
            );
            $this->publishMessage($message, self::RABBIT_EXCHANGE_DEFAULT);
        }
    }

    private function getRecordDomain(array $record) {
        $keys = ['target', 'exchange'];
        foreach ($keys as $key) {
            if (array_key_exists($key, $record)) {
                return $record[$key];
            }
        }
    }

    public function toWebServiceRequest(): string
    {
        return "dnsrecon/$this->host";
    }
}