<?php

declare(strict_types=1);


namespace app\models\commands;

class PhpmyadminCommand extends AbstractCommand
{
    const SCRIPT_NAME = 'websploit';
    
    public $domain;
    public $isHttps;
    
    protected $interestingSubstr = [
        '[301 Moved Permanently]'
    ];

    public function postExecute()
    {
        $lines = explode("\n", $this->output);
        foreach ($lines as &$line) {
            if (empty($line))
            {
                continue;
            }
            $pos =  strpos($line, "/]");            
            if ($pos > 0 && strpos($line, "[93m[/") === 0 && strpos($line, 'Not Found') === false)
            {
                $folder = substr($line, 7, $pos -7);
                $this->debugPrint("Phpmyadmin found something on $this->domain/$folder");
            }                
        }
    }

    /**
     * @param \stdClass $msgBody
     */
    public function initParameters(\stdClass $msgBody)
    {
        parent::initParameters($msgBody);
        $this->isHttps = $msgBody->extra->isHttps;
    }

    public function preExecute()
    {
        $protocol = $this->isHttps ? 'https' : 'http';
        $this->setCommand(self::SCRIPT_NAME." web/pma {$protocol}://{$this->domain}");
    }

    public static function getCommandName() : string
    {
        return 'phpmyadmin';
    }

    public function toWebServiceRequest(): string
    {
        $protocol = $this->isHttps ? 'https' : 'http';
        return "pma/{$protocol}/{$this->domain}";
    }
}
