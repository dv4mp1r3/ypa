<?php

declare(strict_types=1);

namespace app\models\templates;

use app\models\PageSource;

interface ISimilar
{
    /**
     * Сравнение шаблонов друг с другом на предмет похожести
     * @param string $pageSource
     * @return boolean результат проверки
     */
    public function looksLike(string $pageSource) : bool ;
}
