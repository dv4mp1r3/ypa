<?php

declare(strict_types=1);

namespace app\models\payloads;


use app\models\commands;
use app\models\runners\RunnerSettings;
use PhpAmqpLib\Message\AMQPMessage;
use app\exceptions\ValueError;

class TaskProcessor extends AbstractPayload
{
    /**
     * Вывод результата выполнения задачи
     * @var bool
     */
    private $printResult = false;

    /**
     * @var commands\AbstractCommand
     */
    private $command;

    /**
     * @var AMQPMessage
     */
    private $message;

    /**
     * @param \stdClass $msgBody
     * @throws ValueError
     */
    private function validateMessageBody(\stdClass $msgBody) {
        if (!property_exists($msgBody, 'command'))
        {
            throw new ValueError('property command does not exists in message');
        }

        if (!property_exists($msgBody, 'taskId'))
        {
            throw new ValueError('property taskId does not exists in message');
        }

        if (!class_exists($msgBody->command))
        {
            throw new ValueError('Unknown command '.$msgBody->command);
        }
    }

    /**
     *
     * @param AMQPMessage $message
     */
    public function execute(AMQPMessage $message)
    {
        try
        {
            $msgBody = json_decode($message->body);
            $this->validateMessageBody($msgBody);
            // supposed namespace app\\models\\commands\\{$cmd}Command;
            $commandClassname = $msgBody->command;

            /**
             * @var commands\AbstractCommand cmd
             */
            $this->command = new $commandClassname($this->connection);
            $this->command->initParameters($msgBody);
            if ($this->runnerSettings instanceof RunnerSettings) {
                $this->command->setRunnerSettings($this->runnerSettings);
            }
            $this->message = $message;

            $output = $this->command->run();
            if ($this->printResult) {
                echo $output.PHP_EOL;
            }
            $this->sendSuccess($this->message);
        } 
        catch (\Exception $ex) 
        {
            if (!defined('YII_DEBUG') && YII_DEBUG) {
                throw $ex;
            }
            $this->printException($ex);           
        }
    }

    public function getCommand(): AbstractPayload {
        return $this->command;
    }

    public function printResult(bool $print) {
        $this->printResult = $print;
    }
}
