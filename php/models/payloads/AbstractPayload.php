<?php

declare(strict_types=1);

namespace app\models\payloads;

use app\models\runners\RunnerSettings;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use app\models\Notification;

abstract class AbstractPayload
{
    const COMMAND_CONTEXT_WEB = 'web';
    const COMMAND_CONTEXT_CLI = 'cli';
    
    protected $queue;
    
    /**
     *
     * @var AMQPStreamConnection;
     */
    protected $connection;

    /**
     * @var RunnerSettings
     */
    protected $runnerSettings;

    public function setRunnerSettings(RunnerSettings $runnerSettings) {
        $this->runnerSettings = $runnerSettings;
    }
    
    /**
     * 
     * @param AMQPStreamConnection $connection
     */
    public function setConnection(AMQPStreamConnection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * 
     * @param string $queue
     */
    public function __construct(string $queue)
    {
        $this->queue = $queue;
    }

    /**
     * 
     * @param AMQPMessage $message
     */
    abstract public function execute(AMQPMessage $message);

    /**
     *
     * @param AMQPMessage $message
     * @param \stdClass|null $data
     */
    protected function sendSuccess(AMQPMessage $message, \stdClass $data = null)
    {
        if (!is_null($data)) {
            $this->afterExecute($data);
        }

        if (!empty($message->delivery_info) && array_key_exists('channel', $message->delivery_info))
        {
            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
        }
    }
    
    /**
     * 
     * @param \Exception $ex
     */
    protected function printException(\Exception $ex)
    {
        echo $ex->getMessage();
        echo PHP_EOL;
        echo $ex->getTraceAsString();
        echo PHP_EOL;
    }
}
