<?php

declare(strict_types=1);

namespace app\models\payloads;

use app\models\Notification;
use PhpAmqpLib\Message\AMQPMessage;
use app\exceptions\ValueError;

class NotificationProcessor extends AbstractPayload implements IAfterExecute
{
    const AFTER_SAVE_NOTIFY_TO_TELEGRAM = 'telegram';

    /**
     * @param \stdClass $data
     */
    public function afterExecute(\stdClass $data)
    {
        if (!property_exists($data->body->extra, 'afterSave'))
        {
            return;
        }

        if ($data->body->extra->afterSave === self::AFTER_SAVE_NOTIFY_TO_TELEGRAM)
        {
            //todo: отправить уведомление в телеграм
        }
    }

    public function initCommand(AMQPMessage $message)
    {
        $notification = new Notification();

        $msgBody = json_decode($message->body);

        if (!property_exists($msgBody, 'taskId'))
        {
            throw new ValueError('property taskId does not exists in message');
        }

        $this->mapMessageToNotification($notification, $message);
        if (!$notification->validate())
        {
            throw new ValueError("Can't validate notification. Errors: ".$notification->getErrorsAsString());
        }
        $notification->save();
        $this->afterExecute($message);
    }

    /**
     * Мапинг сообщения из очереди в уведомление по списку атрибутов внутри метода
     * @param Notification $notification
     * @param AMQPMessage $message
     */
    protected function mapMessageToNotification(Notification $notification, AMQPMessage $message)
    {
        $attrs = [
            'extra',
            'command',
            'type',
            'level',
            'text'
        ];
        if (!property_exists($message->body, 'extra'))
        {
            throw new \InvalidArgumentException('message does not have extra property');
        }

        foreach ($attrs as $attr)
        {
            if (property_exists($message->body->extra, $attr))
            {
                $notification->setAttribute($attr, $message->body->extra->$attr);
            }
        }
    }

    public function execute(AMQPMessage $message)
    {
        // TODO: Implement execute() method.
    }
}
