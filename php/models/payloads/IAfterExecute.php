<?php

declare(strict_types=1);

namespace app\models\payloads;


interface IAfterExecute
{
    public function afterExecute(\stdClass $data);
}