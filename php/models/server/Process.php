<?php

declare(strict_types=1);

namespace app\models\server;


use app\models\commands\PsCommand;

class Process
{
    /**
     * @var string
     */
    protected $user;

    /**
     * @var int
     */
    protected $pid;

    /**
     * @var float
     */
    protected $cpuPercent;

    /**
     * @var float
     */
    protected $memoryPercent;

    /**
     * @var string
     */
    protected $command;

    /**
     * Process constructor.
     * @param string $psCommandLine
     */
    public function __construct(string $psCommandLine)
    {
        $array = array_values(array_filter(explode(' ', $psCommandLine),
            function ($e) {
                return $e !== '' && $e !== ' ';
            }));
        $arrayCount = count($array);
        if ($arrayCount < PsCommand::MAX_COLUMNS) {
            return;
        }

        if ($arrayCount > PsCommand::MAX_COLUMNS) {
            $this->command = implode(' ', array_slice($array, PsCommand::INDEX_COMMAND));
        } else {
            $this->command = $array[PsCommand::INDEX_COMMAND];
        }

        $this->user = $array[PsCommand::INDEX_USER];
        $this->pid = (int)$array[PsCommand::INDEX_PID];
        $this->cpuPercent = round((float)$array[PsCommand::INDEX_CPU_PERCENT], 1);
        $this->memoryPercent = round((float)$array[PsCommand::INDEX_MEMORY_PERCENT], 1);
    }
    
    public function getCommand(): string
    {
        return $this->command;
    }

    public function getUser(): string
    {
        return $this->user;
    }

    public function getPid(): int
    {
        return $this->pid;
    }

    public function getCpuPercent(): float
    {
        return $this->cpuPercent;
    }

    public function getMemoryPercent(): float
    {
        return $this->memoryPercent;
    }
    
    public static function getGridViewColumnsArray() : array
    {
        return [
            [
                'label' => 'PID',
                'value' => function($model) { return $model->getPid();}, 
            ],
            [
                'label' => 'User',
                'value' => function($model) { return $model->getUser();}, 
            ],
            [
                'label' => 'CPU load',
                'value' => function($model) { return $model->getCpuPercent();}, 
            ],
            [
                'label' => 'Memory load',
                'value' => function($model) { return $model->getMemoryPercent();}, 
            ],
            [
                'label' => 'Command',
                'value' => function($model) { return $model->getCommand();}, 
            ],                 
        ];
    }
}