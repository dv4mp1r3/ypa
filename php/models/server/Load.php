<?php


namespace app\models\server;

/**
 * Сбор информации о нагрузке на сервер
 * @package app\models
 */
class Load
{
    const RESULT_INDEX_USED_PERCENT = 0;
    const RESULT_INDEX_USED_VALUE = 1;

    const RESULT_INDEX_MEMORY_SWAP_PERCENT = 2;
    const RESULT_INDEX_MEMORY_SWAP_VALUE = 3;

    public function parseCpuLoad() : int
    {
        $load = sys_getloadavg();
        return (int)$load[0];
    }

    public function parseMemoryLoad(): array
    {
        $output = [];
        exec("free -mtl", $output);
        $ramArray = $this->calculateMemoryInfoByString(
            $output[1],
            self::RESULT_INDEX_USED_PERCENT,
            self::RESULT_INDEX_USED_VALUE);
        $swapArray = $this->calculateMemoryInfoByString(
            $output[4],
            self::RESULT_INDEX_MEMORY_SWAP_PERCENT,
            self::RESULT_INDEX_MEMORY_SWAP_VALUE);

        return array_merge($ramArray, $swapArray);

    }

    protected function calculateMemoryInfoByString($string, $keyFirst, $keySecond) : array
    {
        $array = array_values(array_filter(explode(' ', $string),
            function($e) {
                return is_numeric($e);
            }));
        $total = (int)trim($array[0]);
        $used =  (int)trim($array[1]);
        $available = $total - $used;

        return [
            $keyFirst => $available === $total ? 0 : (int)round(100/($total/$used)),
            $keySecond => (int)$used
        ];
    }

    public function parseStorageInfo() : array
    {
        $total = disk_total_space ('/');
        $free  = disk_free_space  ('/');
        $used = $total - $free;
        $percent = $used === 0 ? 100 : (int)round(100/($total/$used));

        return [
            self::RESULT_INDEX_USED_PERCENT => $percent,
            self::RESULT_INDEX_USED_VALUE => (int)($used / 1024 / 1024),
        ];
    }

}