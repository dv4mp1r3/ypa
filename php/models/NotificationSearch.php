<?php

declare(strict_types=1);

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\auth\User;

/**
 * NotificationSearch represents the model behind the search form about `app\models\Notification`.
 */
class NotificationSearch extends Notification
{
    const JOIN_TYPE = 'LEFT JOIN';
    const TASK_RELATION = 'task.id = notification.task_id';

    public string $task_name_like = '';

    /**
     * @inheritdoc
     */
    public function rules() : array
    {
        return [
            [['id', 'task_id', 'type', 'level'], 'integer'],
            [['text', 'creation_date', 'extra', 'command', 'task_name_like'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() : array
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params) : ActiveDataProvider
    {
        $query = Notification::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->task_name_like) {
            $query->join(self::JOIN_TYPE, 'task', self::TASK_RELATION)
                ->andFilterWhere(['like', 'task.name', "%{$this->task_name_like}%", false]);
        }

        if ($this->creation_date) {
            $query->andFilterWhere([
                'between',
                'creation_date',
                "$this->creation_date 00:00:00",
                "$this->creation_date 23:59:59"
            ]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'task_id' => $this->task_id,
            'type' => $this->type,
            'level' => $this->level,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'extra', $this->extra])
            ->andFilterWhere(['like', 'command', $this->command]);
                   
        return $dataProvider;
    }
    
    /**
     * Поиск уведомлений текущего пользователя
     * Взятые им из уведомлений админа
     * или созданные задачами  текущего пользователя
     * @param array $params
     * @return ActiveDataProvider
     */
    public function searchForCurrentUserOnly(array $params) : ActiveDataProvider
    {
        $dataProvider = $this->search($params);
        $query = $dataProvider->query;
        
        $query->andFilterWhere(['taken_by_user_id' => User::getCurrentUserId(),]);        
        $query->join(self::JOIN_TYPE, 'task', self::TASK_RELATION)
                ->orFilterWhere(['task.user_id' => User::getCurrentUserId()]);  
        
        return $dataProvider;        
    }
    
    public function searchCreatedByAdmin(array $params) : ActiveDataProvider
    {
        $dataProvider = $this->search($params);
        /**
         * @var ActiveQuery $query
         */
        $query = $dataProvider->query;
        $null = new yii\db\Expression('NULL');
        $query->join(self::JOIN_TYPE, 'task', self::TASK_RELATION)
            ->join(self::JOIN_TYPE, 'user', 'task.user_id = user.id')
            ->andFilterWhere(['user.is_admin' => 1])
            ->andFilterWhere(['is', 'taken_by_user_id',  $null]);
        
        return $dataProvider;
       
    }

    public function getNotificationsCount(array $taskIds, $groupBy = [], bool $indexView = true) : array
    {
        $query = NotificationSearch::find()->addSelect(['count(*) as c', 'level']);
        if ($groupBy !== []) {
            $query->groupBy($groupBy);
        }
        $dbData = $query->where(['in', 'task_id', $taskIds])->asArray(true)->all();
        $result = [];
        array_map(function($el) use (&$result, $indexView) {
            $type = Notification::getLevelByValue(intval($el['level']));
            $arrayKey = $indexView ? "{$type}" : ucfirst($type);
            $result[$arrayKey] = intval($el['c']);
            return 0;
        }, $dbData);
        return $result;
    }
}
