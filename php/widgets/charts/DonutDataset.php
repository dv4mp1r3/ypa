<?php

declare(strict_types=1);

namespace app\widgets\charts;

class DonutDataset extends BaseDataset
{
    const COLOR_NOTIFICATION_UNKNOWN = '#3a3b42';
    const COLOR_NOTIFICATION_LOW = '#858796';
    const COLOR_NOTIFICATION_MEDIUM = '#36b9cc';
    const COLOR_NOTIFICATION_HIGH = '#f6c23e';
    const COLOR_NOTIFICATION_CRITICAL = '#e74a3b';

    public array $backgroundColor = [];

    public array $hoverBackgroundColor = [];

    public string $hoverBorderColor = "rgba(234, 236, 244, 1)";

    /**
     * @throws \Exception
     */
    public function validate()
    {
        $dataCount = count($this->data);
        if ($dataCount !== count($this->backgroundColor)
            || $dataCount !== count($this->hoverBackgroundColor)) {
            throw new \Exception('Properties backgroundColor/hoverBackgroundColor/data must have the same length');
        }
    }

    public function setBackgroundColors(array $colors): DonutDataset
    {
        $this->backgroundColor = $colors;
        return $this;
    }

    public function setHoverBackground(array $colors): DonutDataset
    {
        $this->hoverBackgroundColor = $colors;
        return $this;
    }

}