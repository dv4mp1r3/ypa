<?php

declare(strict_types=1);

namespace app\widgets\charts;


class BaseDataset
{

    public array $data = [];

    public function setData(array $data): BaseDataset
    {
        $this->data = $data;
        return $this;
    }

    public function addData($el): BaseDataset
    {
        $this->data[] = $el;
    }
}