<?php

declare(strict_types=1);

use yii\helpers\Html;
/**
 * @var string $header
 * @var string $id
 * @var string $footer
 * @var string $options
 * @var string $labels
 * @var string $datasets
 * @var string $cardClass
 */
?>
<div class="card shadow <?= Html::encode($cardClass) ?>">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary"><?= Html::encode($header); ?></h6>
    </div>
    <div class="card-body">
        <div class="chart-area">
            <div class="chartjs-size-monitor">
                <div class="chartjs-size-monitor-expand">
                    <div class=""></div>
                </div>
                <div class="chartjs-size-monitor-shrink">
                    <div class=""></div>
                </div>
            </div>
            <canvas id="<?= Html::encode($id); ?>" style="display: block; width: 738px; height: 320px;" width="738"
                    height="320"
                    class="chartjs-render-monitor"></canvas>
        </div>
        <hr>
        <?= Html::encode($footer); ?>
    </div>
</div>
<script>
    const ctx = document.getElementById("<?= Html::encode($id); ?>");
    const chartOptions = <?= $options; ?>;
    const myLineChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: <?= $labels ?>,
            datasets: <?= $datasets ?>
        },
        options: chartOptions
    });
</script>
