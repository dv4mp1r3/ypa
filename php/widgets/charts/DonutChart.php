<?php

declare(strict_types=1);

namespace app\widgets\charts;

use yii\helpers\Json;

class DonutChart extends BaseChart
{
    protected function getViewData(): array
    {
        return parent::getViewData();
    }
}