<?php

declare(strict_types=1);

namespace app\widgets\charts;

use app\models\helpers\BaseOptionsBuilder;
use yii\base\Widget;
use yii\helpers\Json;


class BaseChart extends Widget
{
    private string $header = '';

    private string $footer = '';

    private array $dataSets = [];

    private array $labels = [];

    private string $cardClass = 'mb-4';

    private BaseOptionsBuilder $optionsBuilder;

    public function setHeader(string $header): BaseChart
    {
        $this->header = $header;
        return $this;
    }

    public function setFooter(string $footer): BaseChart
    {
        $this->footer = $footer;
        return $this;
    }

    public function setOptionsBuilder(BaseOptionsBuilder $optionsBuilder): BaseChart
    {
        $this->optionsBuilder = $optionsBuilder;
        return $this;
    }

    public function updateDatasets(array $dataSets): BaseChart
    {
        $this->dataSets = $dataSets;
        return $this;
    }

    public function setLabels(array $labels): BaseChart
    {
        $this->labels = $labels;
        return $this;
    }

    public function setCardClass(string $cardClass) : BaseChart
    {
        $this->cardClass = $cardClass;
    }

    public function run(): string
    {
        return $this->render($this->getViewName(), $this->getViewData());
    }

    private function getViewName(): string
    {
        $chartClassname = get_called_class();
        return str_replace(['app\\widgets\\charts\\', 'chart'], '', mb_strtolower($chartClassname));
    }

    protected function getViewData() : array {
        return [
            'header' => $this->header,
            'id' => $this->getId(),
            'footer' => $this->footer,
            'options' => $this->optionsBuilder->getOptionsAsJson(JSON_PRETTY_PRINT),
            'labels' => Json::encode($this->labels),
            'datasets' => Json::encode($this->dataSets),
            'cardClass' => $this->cardClass,
        ];
    }
}