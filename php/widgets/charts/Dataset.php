<?php

declare(strict_types=1);

namespace app\widgets\charts;

class Dataset extends BaseDataset
{
    public float $lineTension = 0.3;

    public string $backgroundColor = 'rgba(78, 115, 223, 0.05)';

    public string $borderColor = 'rgba(78, 115, 223, 1)';

    public int $pointRadius = 3;

    public string $pointBackgroundColor = 'rgba(78, 115, 223, 1)';

    public string $pointBorderColor = 'rgba(78, 115, 223, 1)';

    public int $pointHoverRadius = 3;

    public string $pointHoverBackgroundColor = 'rgba(78, 115, 223, 1)';

    public string $pointHoverBorderColor = 'rgba(78, 115, 223, 1)';

    public int $pointHitRadius = 10;

    public int $pointBorderWidth = 2;

    public string $label = '';

    public function __construct($label)
    {
        $this->label = $label;
    }

}