<?php

declare(strict_types=1);

use app\widgets\charts\DonutDataset;
use yii\helpers\Html;
use app\models\Notification;
use app\models\helpers\FieldHelper;

/**
 * @var array $levels;
 * @var integer $taskId;
 */
$n = new Notification();
$classByLevel = [
    'unknown' => [
        'a' => 'type-unknown',
        'i' => 'fa-question'
    ],
    'low' =>[
        'a' => 'btn-secondary',
        'i' => 'fa-check'
    ],
    'medium' => [
        'a' => 'btn-info',
        'i' => 'fa-info-circle'
    ],
    'high' => [
        'a' => 'btn-warning',
        'i' => 'fa-exclamation-triangle'
    ],
    'critical' => [
        'a' => 'btn-danger',
        'i' => 'fa-exclamation'
    ],
];
?>
<style>
    .btn-rounded-square {
        height: 2.5rem;
        width: 2.5rem;
        font-size: 1rem;
        display: inline-flex;
        align-items: center;
        justify-content: center;
    }
    .btn-rounded-square i {
        position: relative;
        left: 9px !important;
    }
    .notifications-value {
        position: relative;
        top: 20px !important;
    }
    .badge-notifications-count {
        background: #1d6f4d !important;
    }
    .type-unknown {
        background-color: <?= Html::encode(DonutDataset::COLOR_NOTIFICATION_UNKNOWN) ?>;
    }
</style>
<div class="">
    <?php foreach ($levels as $level => $value) { ?>
        <a href="?r=notification%2Findex&NotificationSearch%5Blevel%5D=<?= FieldHelper::getTypeValueByString($n, 'level', $level); ?>&NotificationSearch%5Btask_id%5D=<?= $taskId ?>"
           class="btn <?= $classByLevel[$level]['a'] ?> btn-rounded-square " target="_blank">
            <i class="fas <?= $classByLevel[$level]['i'] ?>"></i>
            <span class="notifications-value badge badge-danger badge-notifications-count badge-counter"><?= $value ?></span>
        </a>
    <?php } ?>
</div>
