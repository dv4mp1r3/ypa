<?php

use yii\helpers\Html;

/**
 * @var string $bgColor
 * @var string $caption
 * @var string $text
 * @var string $length
 */

?>
<div class="<?= Html::encode($length); ?>">
    <div class="card bg-<?= Html::encode($bgColor); ?> text-white shadow">
        <div class="card-body">
            <?= Html::encode($caption); ?>
            <div class="text-white-50 small"><?= Html::encode($text); ?></div>
        </div>
    </div>
</div>
