<?php

declare(strict_types=1);

namespace app\widgets\notification;

use app\models\NotificationSearch;
use yii\base\Widget;
use app\models\Notification;


class GroupedByTypeWidget extends Widget
{
    public int $taskId;

    public function run(): string {
        $nSearch = new NotificationSearch();
        $levels = $nSearch->getNotificationsCount([$this->taskId], ['level']);
        return $this->render(
            'task-detail-view',
            [
                'levels' => $levels,
                'taskId' => $this->taskId,
            ]
        );
    }
}