<?php

declare(strict_types=1);

namespace app\widgets\notification;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class Notification extends Widget {
    
    const COLOR_PRIMARY = 'primary';
    const COLOR_SECONDARY = 'secondary';
    const COLOR_SUCCESS = 'success';
    const COLOR_INFO = 'info';
    const COLOR_WARNING = 'warning';
    const COLOR_DANGER = 'danger';
    const COLOR_LIGHT = 'light';
    const COLOR_DARK = 'dark';
    
    const DEFAULT_LEN = 'col-lg-6 mb-4';
    
    public $bgColor;
    
    public $caption;
    
    public $text;
    
    public $length = self::DEFAULT_LEN;
    
    public function run(): string {
        return $this->render('main', [
            'bgColor' => $this->bgColor,
            'caption' => $this->caption,
            'text' => $this->text,
            'length' => $this->length,
        ]);
    }
}
