<?php

declare(strict_types=1);

namespace app\widgets\progressbar;

use yii\base\Widget;

class ProgressBar extends Widget
{
    const TYPE_PRIMARY = 'primary';
    const TYPE_SUCCESS = 'success';
    const TYPE_INFO = 'info';
    const TYPE_WARNING = 'warning';

    public $max = 100;

    public $min = 0;

    public $value;

    public $type = '';

    public function run() : string
    {
        return $this->render('main', [
            'min' => $this->min,
            'max' => $this->max,
            'value' => $this->value,
            'type' => $this->type,
        ]);
    }

}