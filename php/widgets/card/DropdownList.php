<?php

declare(strict_types=1);

namespace app\widgets\card;

use yii\base\Widget;

class DropdownList extends Widget {
    
    const MENU_ITEM_DELIMITER = 0;
    const MENU_ITEM_URL = 1;
    
    public $header;
    
    /**
     *
     * @var array 
     */
    protected $menu = [];
    
    public function addDelimiter() : DropdownList
    {
        $this->menu []= [
            'type' => self::MENU_ITEM_DELIMITER
        ];
        return $this;
    }
    
    public function addUrl($url, $text) : DropdownList
    {
        $this->menu []= [
            'type' => self::MENU_ITEM_URL,
            'text' => $text,
            'url' => $url,
        ];
        return $this;
    }
    
    public function run(): string {
        return $this->render('dropdown', ['menu' => $this->menu, 'header' => $this->header]);
    }
}
