<?php

use yii\helpers\Html;
use \app\widgets\card\DropdownList;

/* @var $menu array */
/* @var $header string */

?>
<div class="dropdown no-arrow">
    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
    </a>
    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
        <div class="dropdown-header"><?= Html::encode($header); ?></div>
        <?php foreach($menu as $item) : ?>
            <?php if(!isset($item['type'])) {continue;} ?>
            <?php if ($item['type'] === DropdownList::MENU_ITEM_DELIMITER): ?>
                <div class="dropdown-divider"></div>
            <?php elseif ($item['type'] === DropdownList::MENU_ITEM_URL): ?>
                <a class="dropdown-item" href="<?= Html::encode($item['url']); ?>"><?= Html::encode($item['text']); ?></a>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
</div>
