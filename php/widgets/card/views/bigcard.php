<?php

use yii\helpers\Html;

/* @var $menu array */
/* @var $className string */
/* @var $dropdownList string */
/* @var $text string */
/* @var $image string */
/* @var $imageClass string */
/* @var $imageStyle string */
/* @var $imageAlt string */
/* @var $data string */
/* @var $header string */
/* @var $headerStyle string */

?>
<div class="<?= Html::encode($className); ?>">
    <div class="card shadow mb-4">
        <?php if(mb_strlen($dropdownList) > 0): ?>
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary"><?= Html::encode($header); ?></h6>
            <?= $dropdownList ?>
        </div>
        <?php else: ?>
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary"><?= Html::encode($header); ?></h6>
            </div>
        <?php endif; ?>
        <div class="card-body">
            <?php if (mb_strlen($image) > 0): ?>
                <div class="text-center">
                    <img class="<?= Html::encode($imageClass); ?>"
                         style="<?= Html::encode($imageStyle); ?>"
                         src="<?= Html::encode($image); ?>"
                         alt="<?= Html::encode($imageAlt); ?>">
                </div>
            <?php endif; ?>
            <?= mb_strlen($text) > 0 ? $text : ''; ?>
            <?= mb_strlen($data) > 0 ? $data : ''; ?>
        </div>
    </div>
</div>
