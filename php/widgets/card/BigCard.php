<?php

declare(strict_types=1);

namespace app\widgets\card;

use yii\base\Widget;

class BigCard extends Widget
{

    const CLASS_GRAPHIC_LONG = 'col-xl-8 col-lg-7';
    const CLASS_GRAPHIC_SHORT = 'col-xl-4 col-lg-5';
    const CLASS_HALF_PAGE_PART = 'col-lg-6 mb-4';

    public $className = self::CLASS_HALF_PAGE_PART;

    /**
     * @var DropdownList
     */
    public $dropdownList;

    public $text;

    public $image;

    public $imageStyle = 'width: 25rem;';

    public $imageClass = 'img-fluid px-3 px-sm-4 mt-3 mb-4';

    public $imageAlt;

    public $data;

    public $header;

    public function run(): string
    {
        $dropdownString = $this->dropdownList instanceof DropdownList
            ? $this->dropdownList->run() : '';
        return $this->render('bigcard', [
            'dropdownList' => $dropdownString,
            'text' => $this->text,
            'image' => $this->image,
            'imageClass' => $this->imageClass,
            'imageStyle' => $this->imageStyle,
            'imageAlt' => $this->imageAlt,
            'data' => $this->data,
            'header' => $this->header,
            'className' => $this->className,
        ]);
    }
}
