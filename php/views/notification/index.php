<?php

declare(strict_types=1);

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use app\models\auth\User;
use app\models\helpers\FieldHelper;
use app\models\Notification;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NotificationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notifications';
$this->params['breadcrumbs'][] = $this->title;
$n = new Notification();
$levelValues = FieldHelper::getFieldValues($n, 'level');
$typeValues = FieldHelper::getFieldValues($n, 'type');
?>
<div class="notification-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'short_info',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'creation_date',
                'filter' =>   DatePicker::widget([
                    'name' => 'NotificationSearch[creation_date]',
                    'value' => $searchModel->creation_date,
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ],
                    'convertFormat' => false,
                ]),
            ],
            [
                'attribute' => 'type',
                'format' => 'raw',
                'filter' => $typeValues,
                'value' => function ($model) {
                    return FieldHelper::beautify(FieldHelper::getTypeString($model, 'type', $model->type));
                },
            ],
            [
                'attribute' => 'level',
                'format' => 'raw',
                'filter' => $levelValues,
                'value' => function ($model) {
                    return FieldHelper::beautify(FieldHelper::getTypeString($model, 'level', $model->level));
                },
            ],
            [
                'attribute' => 'task_name_like',
                'label' => 'Task name',
                'format' => 'raw',
                'filter' => Html::textInput(
                        'NotificationSearch[task_name_like]',
                        $searchModel->task_name_like,
                        ['class' => 'form-control']
                ),
                'value' => function ($model) {
                    $task = $model->getTask()->one();
                    return Html::a(
                        Html::encode($task->name),
                        Url::to(['task/view', 'id' => $task->id])
                    );
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'free' => function ($url, $model, $key) {
                        if ($model->taken_by_user_id !== User::getCurrentUserId())
                        {
                            return '';
                        }                       
                        $data = [
                            'notification/free', 
                            'id' => $model->id,
                            ];
                        $icon = 'fa-file-export';
                        return Html::a ( '<span class="fa '.$icon.'" aria-hidden="true"></span> ', $data);
                    },
                ],
                'template' => '{view} {free}'
            ],
            
        ],
    ]); ?>
</div>
