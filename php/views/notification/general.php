<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\auth\User;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NotificationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notifications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notification-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Notification', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'short_info',
            'creation_date',
            'type',
            'level',
            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'take' => function ($url, $model, $key) {
                        if ($model->taken_by_user_id === User::getCurrentUserId())
                        {
                            return '';
                        }
                        
                        $data = [
                            'notification/take', 
                            'id' => $model->id,
                            ];
                        $icon = 'fa-file-import';
                        return Html::a ( '<span class="fa '.$icon.'" aria-hidden="true"></span> ', $data);
                    },
                ],
                'template' => '{take} {free}', 
            ],
        ],
    ]); ?>
</div>
