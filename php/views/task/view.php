<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\auth\User;
use app\models\Task;
use app\widgets\charts\DonutChart;
use app\widgets\card\Card;

/* @var $this yii\web\View */
/* @var $model app\models\Task */
/* @var $domains array */
/* @var $domainsCount integer */
/* @var $chart DonutChart */
/* @var $cardProgress Card */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$currentUserIsAdmin = User::currentUserIsAdmin();

?>
<div class="task-view">
    <div class="row" style="display: flex;">
        <div style="margin-right: 16px;">
            <p>
                <?= Html::a('Back to task list', ['index'], ['class' => 'btn btn-info']) ?>
            </p>
        </div>
        <div>
            <?php if ($domainsCount > 0): ?>
                <p>Successfully pushed <?= $domainsCount ?> domain's to RabbitMQ</p>
            <?php endif; ?>
            <p>
                <?= $model->status !== Task::STATUS_APPROVED && $currentUserIsAdmin && Yii::$app->params['isDemo'] === 0
                    ?  Html::a('Approve', ['approve', 'id' => $model->id], ['class' => 'btn btn-primary'])
                    : '' ?>
                <?= $model->status === Task::STATUS_APPROVED && $currentUserIsAdmin && Yii::$app->params['isDemo'] === 0
                    ? Html::a('Push to queue', ['push', 'id' => $model->id], ['class' => 'btn btn-primary'])
                    : '' ?>
            </p>
        </div>
    </div>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'status',
            'creation_date',
        ],
    ]) ?>
    <div class="row">
        <div class="col-md-6 mb-5">
            <?= $chart->run() ?>
        </div>
        <div class="col-md-6 mb-5">
            <?= $cardProgress->run(); ?>
        </div>


    </div>
    <h2>Domains list</h2>
    <ul>
        <?php foreach ($domains as $domain): ?>
        <li><?= $domain ?></li>           
        <?php endforeach; ?>
    </ul>
</div>
