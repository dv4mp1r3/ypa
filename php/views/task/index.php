<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use app\widgets\notification\GroupedByTypeWidget as NotificationsGroupedByTypeWidget;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tasks';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-index">
    <?php if (empty(Yii::$app->params['isDemo'])): ?>
        <p>
            <?= Html::a('Create Task', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(Html::encode($model->name),
                        Url::to(['task/view', 'id' => $model->id]));
                },
            ],
            [
                'attribute' => 'creation_date',
                'filter' =>   DatePicker::widget([
                    'name' => 'TaskSearch[creation_date]',
                    'value' => $searchModel->creation_date,
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ],
                    'convertFormat' => false,
                ]),
            ],
            [
                'attribute' => 'notifications',
                'format' => 'raw',
                'value' => function ($model) {
                    return (new NotificationsGroupedByTypeWidget(['taskId' => $model->id]))->run();
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
