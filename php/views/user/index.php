<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\auth\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'creation_date',
            'last_login',
            'is_admin',
            'disabled',

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'lock' => function ($url, $model, $key) {
                        $data = [
                            'user/disable', 
                            'id' => $model->id,
                            'disabled' => !$model->disabled,
                            ];
                        $icon = $model->disabled ? 'fa-lock-open' : 'fa-lock';
                        return Html::a ( '<span class="fa '.$icon.'" aria-hidden="true"></span> ', $data );
                    },
                    'set-as-admin' => function ($url, $model, $key) {
                        $data = [
                            'user/set-as-admin', 
                            'id' => $model->id,
                            'isAdmin' => !$model->is_admin,
                            ];
                        $icon = $model->is_admin ? 'fa-user-plus' : 'fa-user-minus';
                        return Html::a ( '<span class="fa '.$icon.'" aria-hidden="true"></span> ', $data );
                    },
                    'login-as' => function ($url, $model, $key) {
                        if (Yii::$app->user->getIdentity()->id === $model->id)
                        {
                            return '';
                        }
                        $data = [
                            'user/login-as', 
                            'id' => $model->id,
                            ];
                        $icon = 'fa-user-secret';
                        return Html::a ( '<span class="fa '.$icon.'" aria-hidden="true"></span> ', $data );
                    },
                ],
                'template' => '{lock} {set-as-admin} {login-as}',               
            ],
        ],
    ]); ?>
</div>
