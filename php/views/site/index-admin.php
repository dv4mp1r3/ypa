<?php

use app\models\runners\CliRunnerSettings;
use yii\data\ArrayDataProvider;
use app\widgets\charts\AreaChart;

$command = new \app\models\commands\PsCommand();
$command->setRunnerSettings(new CliRunnerSettings());
//$command->run();
$provider = new ArrayDataProvider([
        //$command->getRunningProcesses()
    'allModels' => [],
    'sort' => [
        //'attributes' => ['id', 'username', 'email'],
    ],
    'pagination' => [
        'pageSize' => 30,
    ],
]);

/* @var $this yii\web\View */
/* @var $areaChart AreaChart */
/* @var $radarData array */

$this->title = '';
?>
<script src="/js/Chart.min.js"></script>
<div class="site-index">

    <div class="body-content">
        <div class="row">
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-warning shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                    Sent alerts</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">14</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-comments fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <h2 style="text-align: center">Notifications by user task</h2>
        <div class="row">
            <canvas id="radar"></canvas>
        </div>
    </div>
</div>
<script>
    
    /**
     * 
     * @param {string} date
     * @param {string} type
     * @param {integer} limit
     * @param {function} callback
     * @returns {undefined}
     */
    function getServerLoadData(series, date, type, limit, callback)
    {
        return;
        $.ajax({
            url: '<?= \Yii::$app->getUrlManager()->createUrl('server-load/get') ?>',
            type: 'POST',
            dataType: 'json',
            data: {
                date: 'now', 
                type: type, 
                limit: limit,
                'csrf-param': typeof variable !== 'yii' ? yii.getCsrfParam() : '',
                'csrf-token': typeof variable !== 'yii' ? yii.getCsrfToken() : ''
            },
            success: function (data) {
                //console.log('success', data[0]);
                callback(data);
            }
        });
    }
    const ctx = document.getElementById("radar");
    const options = {
        scale: {
            angleLines: {
                display: true
            },
            ticks: {
                suggestedMin: 1,
                suggestedMax: <?= intval($radarData['max']) ?>
            }
        }
    };
    const data = {
        labels: <?= json_encode($radarData['labels']); ?>,
        datasets: <?= json_encode($radarData['datasets']); ?>
    }
    const myRadarChart = new Chart(ctx, {
        type: 'radar',
        data: data,
        options: options
    });
</script>
