<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\assets\SbAppAsset;

SbAppAsset::register($this);

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="bg-gradient-primary">
    <?php $this->beginBody() ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-5 col-lg-6 col-md-9">
                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <div class="">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                                </div>
                                <?php $form = ActiveForm::begin([
                                    'id' => 'login-form',
                                    'options' => [
                                        'class' => 'user',
                                    ],
                                    'fieldConfig' => [
                                        'template' => "<div class=\"form-group\">{input}</div>",

                                    ],
                                ]); ?>
                                <?= $form->field($model, 'username')->textInput([
                                    'autofocus' => true,
                                    'class' => 'form-control form-control-user',
                                    'aria-describedby' => '',
                                    'placeholder' => 'Enter username...',
                                ]) ?>
                                <?= $form->field($model, 'password')->passwordInput([
                                    'autofocus' => true,
                                    'class' => 'form-control form-control-user',
                                    'aria-describedby' => '',
                                    'placeholder' => 'Password',
                                ]) ?>
                                <?= $form->field($model, 'rememberMe')->checkbox([
                                    'class' => 'custom-control-input',
                                    'template' => "<div class=\"custom-control custom-checkbox small\">{input}{label}{error}</div>",
                                ])->label('Remember Me', ['class' => 'custom-control-label']) ?>
                                <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-user btn-block', 'name' => 'login-button']) ?>
                                <hr>
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>