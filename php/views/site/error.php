<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
$urlManager = \Yii::$app->getUrlManager();
$identity = Yii::$app->user->getIdentity();

$this->title = $name;
?>
<div class="text-center">
    <div class="error mx-auto" data-text="<?= Html::encode($exception->statusCode ); ?>"><?= Html::encode($exception->statusCode ); ?></div>
    <p class="lead text-gray-800 mb-5">Page Not Found</p>
    <p class="text-gray-500 mb-0">It looks like you found a glitch in the matrix...</p>
    <?php if ($identity instanceof \app\models\auth\User): ?>
        <a href="<?= $urlManager->createUrl('site/index') ?>">&larr; Back to Dashboard</a>
    <?php else: ?>
        <a href="<?= $urlManager->createUrl('site/login') ?>">&larr; Please login to view dashboard</a>
    <?php endif; ?>
</div>
