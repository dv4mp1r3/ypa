<?php
declare(strict_types=1);

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = '';
$urlManager = \Yii::$app->getUrlManager();
?>
<style>
    .sticky-footer {
        position: fixed;
        width: 100%;
        bottom: 0;
    }
</style>
<div class="site-index">

    <div class="jumbotron">
        <h1>Guest index page</h1>
        <?php if (Yii::$app->params['isDemo'] !== 0): ?>
            <h2>This is a demo environment</h2>
            <h3>Use <?= Html::encode(getenv('YPA_DEMO_USERNAME').':'.getenv('YPA_DEMO_PASSWORD')) ?> to login</h3>
        <?php endif; ?>

        <p><a class="btn btn-lg btn-success" href="<?= $urlManager->createUrl('site/login'); ?>">Login</a></p>
    </div>

    <!--<div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2></h2>
                <p></p>
            </div>
            <div class="col-lg-4">
                <h2></h2>
                <p></p>
            </div>
            <div class="col-lg-4">
                <h2></h2>
                <p></p>
            </div>
        </div>

    </div>-->
</div>
