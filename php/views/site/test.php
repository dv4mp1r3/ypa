<?php

use app\widgets\notification\Notification;
use \app\widgets\card\BigCard;
use yii\helpers\Html;

$this->title = '1234';

?>
<div class="row">

    <?= \app\widgets\card\Card::widget([
        'icon' => 'fa-calendar',
        'text' => '86%',
        'header' => 'Tasks',
        'cardType' => \app\widgets\card\Card::TYPE_SUCCESS,
        'progressBar' => new \app\widgets\progressbar\ProgressBar(['value' => 86]),
    ]); ?>
</div>

<div class="row">
    <?= BigCard::widget([
        'className' => BigCard::CLASS_GRAPHIC_LONG,
        'text' => 'TestText',
        'header' => 'Header',
    ])
    ?>
</div>

<!-- Content Row -->
<div class="row">
    <!-- Content Column -->
    <div class="col-lg-6 mb-4">
        <div class="row">
            <?= Notification::widget([
                    'bgColor' => Notification::COLOR_DARK, 
                    'caption' => 'Test',
                    'text' => 'text',
                ]);
            ?>        
        </div>
    </div>
</div>