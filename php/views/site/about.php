<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use kartik\markdown\Markdown;
use app\assets\SiteAboutAsset;

SiteAboutAsset::register($this);

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= Markdown::convert(file_get_contents(Yii::$app->getBasePath().'/assets/md/readme.md')); ?>
</div>
<div class="site-about">
    <div id="bpmn"></div>
</div>