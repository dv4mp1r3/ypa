<?php

declare(strict_types=1);

namespace app\controllers;

use Yii;
use app\models\auth\User;
use app\models\auth\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use app\models\LoginForm;
use app\models\auth\AccessControlAdminOnly;

class UserController extends BaseController {
    
    /**
     * @inheritdoc
     */
    public function behaviors() : array
    {
        return [
            'access' => [
                'class' => AccessControlAdminOnly::className(),
                'only' => ['index', 'update', 'disable', 'set-as-admin', 'create', 'login-as'],
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'disable', 'set-as-admin', 'create', 'login-as'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['GET'],
                    'disable' => ['GET'],
                    'set-as-admin' => ['GET'],
                ],
            ],
        ];
    }
    
    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex() : string
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id) : User
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate(int $id) : Response
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                //'domains' => $model->readDomains(),
            ]);
        }
    }
    
    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionDisable(int $id, bool $disabled): string
    {
        User::updateAll(['disabled' => $disabled], 'id = '.$id);         
        return $this->actionIndex();
    }
    
    public function actionSetAsAdmin(int $id, bool $isAdmin): string
    {
        User::updateAll(['is_admin' => $isAdmin], 'id = '.$id);        
        return $this->actionIndex();
    }
    
    public function actionLoginAs(int $id)
    {
        /**
         * @var User $user
         */
        $user = Yii::$app->user->getIdentity();
        if ($user === null) {
            return $this->redirect(['site/index']);
        }

        \Yii::$app->getSession()->set(User::LOGIN_AS_SESSION_KEY, $id);
        \Yii::$app->getSession()->set(User::ADMIN_ID_SESSION_KEY, $user->id);
        $model = new LoginForm();
        $model->loginAs($id);
        return $this->goHome();        
    }
}
