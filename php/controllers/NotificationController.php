<?php

declare(strict_types=1);

namespace app\controllers;

use app\exceptions\ValueError;
use app\models\helpers\DemoRulesHelper;
use Yii;
use app\models\Notification;
use app\models\NotificationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\auth\User;
use yii\filters\AccessControl;

/**
 * NotificationController implements the CRUD actions for Notification model.
 */
class NotificationController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors() : array
    {
        $availActions = ['index', 'view', ];
        $nonDemoActions = ['take', 'free', 'general'];
        $rulesHelper = new DemoRulesHelper($availActions, $nonDemoActions);
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => $rulesHelper->getAllActions(),
                'rules' => $rulesHelper->getRules(),
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Notification models.
     * @return mixed
     */
    public function actionIndex() : string
    {
        $searchModel = new NotificationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Notification model.
     * @param integer $id
     * @return mixed
     */
    public function actionView(int $id) : string
    {
        $model = $this->findModel($id);
        $task = $model->getTask()->one();       
        $userId = User::getCurrentUserId();
        
        if ($model->taken_by_user_id !== $userId && $task->user_id !== $userId)
        {
            throw new ValueError('Can not get access to record');
        }
        
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Notification model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Notification the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id) : Notification
    {
        if (($model = Notification::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
     * 
     * @param int $id
     * @return string
     * @throws \LogicException
     */
    public function actionTake(int $id) 
    {
        try
        {
            $model = $this->findModel($id);
            $user = User::getCurrentUser();
            
            //Если уже взято - редиректим
            if ($model->taken_by_user_id === $user->id)
            {
                return $this->redirect(['view', 'id' => $id]);
            }
            
            if ($user->isAdmin())
            {
                throw new \LogicException('User is admin');
            }
            if (!$model->isCreatedByAdmin())
            {
                throw new \LogicException('Motification is not created by admin task');
            }

            $model->taken_by_user_id = $user->id;
            if (!$model->validate())
            {
                throw new \LogicException('Model->validate() error');
            } 
            $model->save();
            return $this->redirect(['view', 'id' => $id]);
        } catch (\Exception $ex) {
            \Yii::error(__METHOD__.": ".$ex->getMessage());
            return $this->redirect(['general']);
        }          
    }
    
    /**
     * 
     * @param integer $id
     * @return type
     */
    public function actionFree(int $id)
    {
        try
        {
            $user = User::getCurrentUser();
            if ($user->isAdmin())
            {
                throw new \LogicException('User is admin');
            }
            
            $model = $this->findModel($id);
            if ($model->taken_by_user_id !== User::getCurrentUserId())
            {
                return $this->redirect(['index']);
            }
            
            $model->taken_by_user_id = null;
            $model->save();
                    
        } catch (\Exception $ex) {
            \Yii::error(__METHOD__.": ".$ex->getMessage());
        }
        return $this->redirect(['general']);
    }
    
    /**
     * Страница уведомлений, которые были созданы от задач администраторов
     * @return string
     */
    public function actionGeneral() : string
    {
        $searchModel = new NotificationSearch();
        $dataProvider = $searchModel->searchCreatedByAdmin(Yii::$app->request->queryParams);

        return $this->render('general', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
