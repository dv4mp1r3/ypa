<?php

declare (strict_types=1);

namespace app\controllers;

use app\models\server\LoadRecord;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\Response;

class ServerLoadController extends BaseController
{

    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    // allow only POST requests
                    [
                        'allow' => true,
                        'verbs' => ['POST'],
                        'roles' => ['@']
                    ],
                ],
            ],
        ];
    }

    public function actions(): array
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     *
     * @param string $id
     * @param array $params
     * @return mixed
     * @throws \yii\base\InvalidRouteException
     */
    public function runAction($id, $params = [])
    {
        if ($id === 'get') {
            $params = yii\helpers\ArrayHelper::merge(Yii::$app->request->post(), $params);
        }
        return parent::runAction($id, $params);
    }

    /**
     *
     * @param string $date
     * @param string $type
     * @param int $limit
     * @return string
     */
    public function actionGet(string $date, string $type, int $limit = 19): string
    {
        if ($date === 'now') {
            $date = date_create('now', new \DateTimeZone(\Yii::$app->timeZone))->format('Y-m-d H:i:s');
        }
        $data = LoadRecord::findByDateAndType($date, $type, $limit);
        return json_encode($data);
    }

}
