<?php

declare(strict_types=1);

namespace app\controllers;

use app\exceptions\ValueError;
use app\models\helpers\DemoRulesHelper;
use app\models\Notification;
use app\models\NotificationSearch;
use app\widgets\card\Card;
use app\widgets\progressbar\ProgressBar;
use Yii;
use app\models\Task;
use app\models\TaskSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use app\models\auth\User;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use app\widgets\charts\DonutDataset;
use app\widgets\charts\DonutChart;
use app\models\helpers\PieChartOptionsBuilder;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends BaseController
{
    /**
     *
     * @var string 
     */
    protected static $nonAdminMesage = 'текущий пользователь не является администратором';

    /**
     * @inheritdoc
     */
    public function behaviors() : array
    {
        $availActions = ['index', 'view', ];
        $nonDemoActions = ['create', 'update', 'approve', 'push',];
        $rulesHelper = new DemoRulesHelper($availActions, $nonDemoActions);
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => $rulesHelper->getAllActions(),
                'rules' => $rulesHelper->getRules(),
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex() : string
    {
        $searchModel = new TaskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Task model.
     * @param integer $id
     * @return mixed
     */
    public function actionView(int $id) : string
    {
        $model = $this->findModel($id);
        $chart = new DonutChart();
        $chart->setHeader('Notifications by type');
        $nSearch = new NotificationSearch();
        $nData = $nSearch->getNotificationsCount([$model->id], ['level'], false);
        $colors = $this->getDonutColorsByNotificationLevel($nData);
        $chart->setLabels(array_keys($nData));
        $chart->setOptionsBuilder(new PieChartOptionsBuilder());
        $chart->updateDatasets([
            (new DonutDataset())
                ->setData(array_values($nData))
                ->setBackgroundColors($colors)
                ->setHoverBackground($colors)

        ]);
        $cardProgress = new Card();
        //todo: move to controller, get info from db
        $cardProgress->cardType = Card::TYPE_INFO;
        $cardProgress->header = 'Task progress';
        $cardProgress->text = '36%';
        $cardProgress->progressBar = new ProgressBar();
        $cardProgress->progressBar->type = ProgressBar::TYPE_INFO;
        $cardProgress->progressBar->value = 36;
        return $this->render('view', [
            'model' => $model,
            'domains' => $model->readDomains(),
            'domainsCount' => 0,
            'chart' => $chart,
            'cardProgress' => $cardProgress,
        ]);
    }

    /**
     * Возврат набора цветов для найденных уведомлений
     * @param array $nData
     * @return array
     */
    private function getDonutColorsByNotificationLevel(array $nData): array
    {
        $result = [];
        $colors = [
            DonutDataset::COLOR_NOTIFICATION_UNKNOWN,
            DonutDataset::COLOR_NOTIFICATION_LOW,
            DonutDataset::COLOR_NOTIFICATION_MEDIUM,
            DonutDataset::COLOR_NOTIFICATION_HIGH,
            DonutDataset::COLOR_NOTIFICATION_CRITICAL
        ];
        $nLevels = Notification::getLevelsAsArray();
        for ($i = 0; $i < count($nLevels); $i++) {
            if (array_key_exists($nLevels[$i], $nData)) {
                $result[] = $colors[$i];
            }
        }
        return $result;
    }

    /**
     * Creates a new Task model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \Exception
     */
    public function actionCreate()
    {
        $model = new Task();

        if ($model->load(Yii::$app->request->post())) {
            $model->status = Task::STATUS_CREATED;
            $tmpDomainsFile = UploadedFile::getInstance($model, 'domains');
            if ($tmpDomainsFile !== null) {
                $model->filename = $model->uploadFile($tmpDomainsFile);
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                //'domains' => $model->readDomains(),
            ]);
        }
    }

    /**
     *
     * @param int $id
     * @return mixed
     * @throws \Exception
     */
    public function actionApprove(int $id)
    {
        if (!User::currentUserIsAdmin())
        {
            \Yii::error(__METHOD__.": ".self::$nonAdminMesage);
            return $this->redirect(['view', 'id' => $id]);
        }
        Task::updateAll(['status' => Task::STATUS_APPROVED], ['=', 'id', $id]);
        return $this->redirect(['view', 'id' => $id]);
    }
    
    /**
     * 
     * @param integer $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPush(int $id) : string
    {
        $model = $this->findModel($id);
        if (!User::currentUserIsAdmin())
        {
            \Yii::error(__METHOD__.": ".self::$nonAdminMesage);
            return $this->redirect(['view', 'id' => $model->id]);
        }
        
        if ($model->status !== Task::STATUS_APPROVED)
        {
            throw new ValueError('Task must be in "approved" status');
        }
        if (!$model)
        {
            throw new NotFoundHttpException('Not found task with id '.$id);
        }
        
        $domainsCount = $model->pushToQueue('task');
        return $this->render('view', [
            'model' => $model,
            'domains' => $model->readDomains(),
            'domainsCount' => $domainsCount,
        ]);      
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(int $id) : Task
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
