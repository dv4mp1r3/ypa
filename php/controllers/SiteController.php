<?php

declare(strict_types=1);

namespace app\controllers;

use app\models\auth\User;
use app\models\Notification;
use app\models\NotificationSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use yii\web\Response;
use app\widgets\charts\AreaChart;
use app\models\helpers\AreaChartOptionsBuilder;
use yii\web\JsExpression;
use app\widgets\charts\Dataset as ChartDataset;

class SiteController extends BaseController
{
    public function behaviors() : array
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions() : array
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id === 'login') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionIndex() : string
    {
        //$tmp = shell_exec("/home/test/p0f-mtu/p0f-client /home/test/p0f-socket {$_SERVER['REMOTE_ADDR']}");
        /**
         * @var User $user
         */
        $user = Yii::$app->user->getIdentity();
        if ($user === null) {
            return $this->render('index-guest');
        }

        if ($user->isAdmin())
        {
            return $this->renderAdminIndex();
        }

        return $this->render('index-user');
    }

    private function getRadarData() : array
    {
        $nSearch = new NotificationSearch();
        $taskIds = [
            [
                'id' => 16,
                'name' => 'My Cloud',
                'color' => '#8bc34a',
            ],
            [
                'id' => 17,
                'name' => 'Test WP site',
                'color' => '#2196f3',
            ],
            [
                'id' => 19,
                'name' => 'Test task 10',
                'color' => '#c7954a',
            ],
        ];
        $levels = Notification::getLevelsAsArray();
        $max = 0;
        $datasets = [];
        foreach ($taskIds as $arr) {
            $levelsCount['data'] = $nSearch->getNotificationsCount([$arr['id']], ['level'], false);
            $levelsCount['label'] = $arr['name'];
            $levelsCount['borderColor'] = $arr['color'];

            $levelsCount['data']['Unknown'] = rand(0, 10);
            foreach ($levels as $level) {
                if (!array_key_exists($level, $levelsCount)) {
                    $levelsCount[$level] = 0;
                } else if ($levelsCount[$level] > $max) {
                    $max = $levelsCount[$level];
                }
            }
            $levelsCount['data'] = array_values($levelsCount['data']);
            $datasets[] = $levelsCount;
        }
        return [
            'labels' => $levels,
            'datasets' => $datasets,
            'max' => $max,
        ];
    }

    private function renderAdminIndex() : string
    {
        $b = new AreaChartOptionsBuilder();
        $b->setTooltipLabelCallback(new JsExpression(AreaChartOptionsBuilder::DEFAULT_TOOLTIP_LABEL_CALLBACK));
        $b->setYAxesValueFormat(new JsExpression(AreaChartOptionsBuilder::DEFAULT_Y_AXES_FORMAT));
        $areaChart = new AreaChart();
        $areaChart->setHeader('Notifications summary');
        $areaChart->setOptionsBuilder($b);
        $dateNow = new \DateTime();
        $nData = NotificationSearch::find()
            ->select(['count(*) as c', 'level', 'creation_date', 'datediff(now(), creation_date) as dd'])
            ->where(['<=', 'creation_date', $dateNow->format('Y-m-d H:i:s')])
            ->andWhere(['>=', 'creation_date', $dateNow->sub(new \DateInterval('P1M'))->format('Y-m-d H:i:s')])
            ->groupBy(['dd', 'level'])
            ->orderBy(['creation_date' => 'ASC'])
            ->asArray()
            ->all();
        $chartDatasets = [];
        $dates = [];
        foreach ($nData as $el) {
            $ymdStr = mb_substr($el['creation_date'], 0, mb_strpos($el['creation_date'], ' '));
            $dates[] = $ymdStr;
            $level = $el['level'];
            if (!array_key_exists($level, $chartDatasets)) {
                $chartDatasets[$level] = [];
            }
            $chartDatasets[$level][] = intval($el['c']);
        }
        $areaChart->setLabels($dates);
        $tmp = [];
        foreach ($chartDatasets as $k => $v) {
            $tmp[$k] = (new ChartDataset('label_'.$k))->setData($v);
        }
        $areaChart->updateDatasets([
            (new ChartDataset('label1'))->setData([1, 4, 5, 10, 44, 30, 12, 22, 12, 10, 2, 5]),
            (new ChartDataset('label2'))->setData([0, 2, 4, 1, 3, 3, 2, 2, 5, 8, 2, 1]),
        ]);
        return $this->render(
            'index-admin',
            [
                'areaChart' => $areaChart,
                'radarData' => $this->getRadarData()
            ]
        );
    }

    public function actionLogin() : Response
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        $response = new Response();
        $response->content = $this->renderPartial('login', [
            'model' => $model,
        ]);
        return $response;
    }

    public function actionLogout() : Response
    {    
        $sessionAdminId = \Yii::$app->getSession()->get(User::ADMIN_ID_SESSION_KEY);
        Yii::$app->user->logout();       
        if (intval($sessionAdminId) > 0)
        {
            $model = new LoginForm();
            if (!$model->loginAs($sessionAdminId))
            {
                \Yii::error(__METHOD__.": не удалось вернуться в сессию админа после выхода из пользователя.");
            }
            else
            {
                \Yii::$app->getSession()->set(User::ADMIN_ID_SESSION_KEY, null);
            }
        }

        return $this->goHome();
    }

    public function actionAbout() : string
    {
        return $this->render('about');
    }
    
    public function actionInfo() : string
    {
        return $this->render('info');
    }

    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;
        $this->layout = 'sbadmin';
        if ($exception instanceof \Exception)
        {
            return $this->render('error', ['exception' => $exception]);
        }
        return $this->goHome();
    }
}
