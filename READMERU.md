**Как это работает:**

Доступно [здесь](https://gitlab.com/dv4mp1r3/ypa/-/blob/master/php/assets/md/readme.md).

**Как запустить:**
1. Установите Docker и Docker-compose для запуска данного проекта.
2. Скопируйте файл с vars.env.example и назначьте ему имя vars.env (также не забудьте изменить содержимое файла).
3. Запустите скрипт rabbit_definitions_get.py и cookie_validation_key_gen.py, который расположен в директории ./configs, с помощью следующей команды:

```
cd configs && python3 rabbit_definitions_gen.py && python3 cookie_validation_key_gen.py
```

4. Запустите docker-compose.yaml в корне директории с помощью следующей команды:

```
docker-compose -f docker-compose.yaml up
```

5. Добавьте строку `127.0.0.1 ypa.local` в ваш host файл.

6. Откройте [ссылку](http://ypa.local:8005/?r=site/login) и введите логин/пароль, которые были указаны в файле env, в параметре `YPA_ADMIN_`.

**Пример тела сообщения rabbitmq**
```
"{"taskId":4,"domain":"test.domain","command":"host","extra":null}"
```

**Базовая команда воркера**
```
sudo -u www-data php /var/www/yii rabbit/task
```