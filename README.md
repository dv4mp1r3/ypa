**How it works**

Available [here](https://gitlab.com/dv4mp1r3/ypa/-/blob/master/php/assets/md/readme.md) on russian

**How to start**
1. Install [docker](https://docs.docker.com/engine/install/) and [docker-compose](https://docs.docker.com/compose/install/) to the system.
2. In *configs* folder copy `vars.env.example` to `vars.env` and update each variable (change username and passwords)
   ```cp ypa/configs/vars.env.example ypa/configs/vars.env```.
3. Run `rabbit_definitions_gen.py` to generate definitions for rabbit and `cookie_validation_key_gen.py` for php_web service
   ```cd ypa/configs/ && python3 rabbit_definitions_gen.py && python3 cookie_validation_key_gen.py```.
4. Run docker-compose application from project root.
   ```docker-compose -f deploy-docker-compose.yaml up```
5. Add `127.0.0.1 ypa.local` to hosts file on the system. Path depends on used OS. For example, /etc/hosts on linux based OS or %WINDIR%/system32/drivers/etc/hosts on Windows. 
6. Open [this link](http://ypa.local:8005/?r=site/login) and use user/password from env file with `YPA_ADMIN_` prefix to log in.

**Run dev version**

Make sure that debug environment variables set to correct values.
```
YII_DEBUG=1
YII_ENV=dev
```
In step 4 from "How to start" replace command to ```docker-compose -f developer-docker-compose.yaml up```.
After starting the containers you need to apply migration manually by execute the following command in php_cli container ```cd ypa.local && php yii migrate --interactive=0```.

**Run unit tests**

Execute the following command php_cli container ```cd ypa.local && vendor/bin/codecept run unit```.

**Demo version**

You can also run the application in demo mode. It runs only web components without any data manipulation (read only).
To run in demo mode set IS_DEMO environment variable to 1 and execute ```docker-compose -f demo-docker-compose.yaml up```.

**Example of rabbitmq message body**
```
"{"taskId":4,"domain":"test.domain","command":"host","extra":null}"
```

**Worker basic command**
```
sudo -u www-data php /var/www/yii rabbit/task
```