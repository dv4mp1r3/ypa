FROM php:7.4-cli-alpine3.12 AS php74-base

RUN apk add --no-cache $PHPIZE_DEPS git \
    && docker-php-ext-configure pcntl --enable-pcntl \
    && docker-php-ext-configure sockets --enable-sockets \
    && docker-php-ext-install pcntl sockets mysqli pdo pdo_mysql bcmath \
    && apk add --no-cache \ 
       freetype \
       libjpeg-turbo \
       libpng \
       freetype-dev \
       libjpeg-turbo-dev \
       libpng-dev \
    && docker-php-ext-configure gd \
       --with-freetype=/usr/include/ \
       --with-jpeg=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-enable gd \
    && apk del --no-cache \
       freetype-dev \
       libjpeg-turbo-dev \
       libpng-dev \
    && rm -rf /tmp/*

FROM php74-base AS php74-cli

COPY --from=composer/composer /usr/bin/composer /usr/bin/composer

WORKDIR /var/www

COPY ./php /var/www/ypa.local

RUN addgroup -S cli \
    && adduser \
    --disabled-password \
    --gecos "" \
    --home /home/cli \
    --ingroup cli \
    --uid "1000" \
    cli \
    && mkdir -p ypa.local/uploads \
    && cd ypa.local \ 
    && composer install \
    && chown -R cli:cli ../ypa.local

USER cli

CMD ["php"]
