FROM php:7.4-fpm-alpine AS php74-base

RUN apk add --no-cache $PHPIZE_DEPS git \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug \
    && docker-php-ext-configure pcntl --enable-pcntl \
    && docker-php-ext-configure sockets --enable-sockets \
    && docker-php-ext-install pcntl sockets mysqli pdo pdo_mysql bcmath \
    && apk add --no-cache \
       freetype \
       libjpeg-turbo \
       libpng \
       freetype-dev \
       libjpeg-turbo-dev \
       libpng-dev \
    && docker-php-ext-configure gd \
       --with-freetype=/usr/include/ \
       --with-jpeg=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-enable gd \
    && apk del --no-cache \
       freetype-dev \
       libjpeg-turbo-dev \
       libpng-dev \
    && rm -rf /tmp/*

FROM php74-base AS php74-web

WORKDIR /var/www

COPY --from=composer/composer /usr/bin/composer /usr/bin/composer

RUN addgroup -S web \
    && adduser \
    --disabled-password \
    --gecos "" \
    --home /home/web \
    --ingroup web \
    --uid "1000" \
    web \
    && mkdir -p ypa.local/web/assets \
    && chown -R web:web ypa.local

USER web

CMD ["php-fpm"]