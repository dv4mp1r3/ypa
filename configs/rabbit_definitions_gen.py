#!/usr/bin/env python3
from __future__ import print_function
import base64
import os
import hashlib


def gen_pass_hash(password: str) -> str:
    salt = os.urandom(4)
    tmp0 = salt + password.encode('utf-8')
    tmp1 = hashlib.sha256(tmp0).digest()
    salted_hash = salt + tmp1
    pass_hash = base64.b64encode(salted_hash)
    return pass_hash.decode('utf-8')


def parse_env_file(filepath: str) -> dict:
    result = {}
    with open(filepath, 'r') as f:
        strings = f.read().split('\n')
        for s in strings:
            param = s.split('=')
            if len(param) == 2:
                result[param[0]] = param[1]
        f.close()
    return result


def replace_values_in_file(filepath: str, new_file: str, params: dict):
    result = ''
    if os.path.isfile(new_file):
        os.remove(new_file)
    with open(filepath, 'r') as f:
        result = f.read()
        f.close()
    for name in params:
        if name == 'RABBITMQ_DEFAULT_PASS':
            params[name] = gen_pass_hash(params[name])
        result = result.replace(name, params[name])
    with open(new_file, 'w') as f:
        f.write(result)
        f.close()


envs = parse_env_file('vars.env')
replace_values_in_file('rabbit_definitions.json', 'rabbit_definitions_generated.json', envs)
