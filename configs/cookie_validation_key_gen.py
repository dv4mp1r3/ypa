import os
import string
import random

file = '../php/config/cookieValidationKey.php'
key_len = 52
key = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(key_len))
if os.path.isfile(file):
    os.remove(file)
with open(file, 'w') as f:
    f.write(f"<?php return '{key}';")
    f.close()
